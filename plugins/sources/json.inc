<?php

/**
 * @file
 * Support for migration from Naturalist JSON sources.
 */

/**
 * {@inheritdoc}
 */
class InaturalistMigrateItemJSON extends MigrateItemJSON {

  protected $originalItemUrl;

  /**
   * {@inheritdoc}
   */
  public function __construct($item_url, $item_file, $http_options = []) {
    parent::__construct($item_file, $http_options);

    $this->originalItemUrl = $item_url;
  }

  /**
   * {@inheritdoc}
   */
  public function getItem($id) {
    $item_file = $this->constructItemUrl($id);
    $item_url = $this->constructItemUrl($id, $this->originalItemUrl);

    if (!file_exists($item_file)) {
      migrate_instrument_start("Retrieve $item_url");
      try {
        $json = @file_get_contents($item_url);
        $json = $this->enrichJson($json);
      }
      catch (Exception $e) {
        //+todo: log error
        // print $e;
        watchdog('mgis_inaturalist', $e, array(), WATCHDOG_DEBUG);
      }
      migrate_instrument_stop("Retrieve $item_url");
      if (!empty($json)) {
        file_unmanaged_save_data($json, $item_file, FILE_EXISTS_REPLACE);
      }
      else {
        return NULL;
      }
      //+todo: log empty json
    }

    try {
      $item = parent::getItem($id);
    }
    catch (Exception $e) {
      //+todo: log errors.
      watchdog('mgis_inaturalist', $e, array(), WATCHDOG_DEBUG);
    }

    return $item;
  }

  /**
   * {@inheritdoc}
   */
  protected function constructItemUrl($id, $url = NULL) {
    $url = $url ? $url : $this->itemUrl;
    // Make sure we use an array.
    if (!is_array($id)) {
      $id = array($id);
    }
    return preg_replace(array_fill(0, count($id), '/:id/'), $id, $url, 1);
  }

  /**
   * Enrich iNaturalist data.
   *
   * Add fields like country name and country code. This could be done in a
   * prepareRow() method, but we choose to do it just once for the sake of
   * performance.
   *
   * @param string $json
   *   JSON for an iNaturalist observation.
   *
   * @return string
   *   Enriched JSON.
   */
  protected function enrichJson($json) {
    $data = drupal_json_decode($json);

    if ($data !== NULL && !empty($data['latitude']) && !empty($data['longitude'])) {
      $place_url = url('https://www.inaturalist.org/places.json', [
        'query' => [
          'place_type' => 'Country',
          'latitude' => $data['latitude'],
          'longitude' => $data['longitude'],
        ],
      ]);
      $place_json = file_get_contents($place_url);
      if ($place_json) {
        $place_data = drupal_json_decode($place_json);
        if ($place_data !== NULL && !empty($place_data[0]['code'])) {
          $data['country_code'] = mgis_country_a2_to_a3($place_data[0]['code']);
        }
      }
    }

    return drupal_json_encode($data);
  }

}

/**
 * {@inheritdoc}
 */
class InaturalistMigrateListJSON extends MigrateListJSON {

  protected $observationDirectory;
  protected $photoDirectory;
  protected $observationListFile;
  protected $photoListFile;

  /**
   * {@inheritdoc}
   */
  public function __construct($list_url, $cache_directory, $type) {
    $this->observationDirectory = $cache_directory . '/observations';
    $this->photoDirectory = $cache_directory . '/photos';
    $this->observationListFile = $this->observationDirectory . '/list.json';
    $this->photoListFile = $this->photoDirectory . '/list.json';
    file_prepare_directory($this->observationDirectory, FILE_CREATE_DIRECTORY);
    file_prepare_directory($this->photoDirectory, FILE_CREATE_DIRECTORY);

    if (!file_exists($this->observationListFile)) {
      migrate_instrument_start("Retrieve $list_url");
      $this->getAllJsonIds($list_url);
      migrate_instrument_stop("Retrieve $list_url");
    }

    $list_file = ($type == 'observation') ? $this->observationListFile : $this->photoListFile;
    parent::__construct($list_file);
  }

  /**
   * Get all JSON IDs through all pages.
   */
  protected function getAllJsonIds($list_url) {
    $page = $total_pages = 1;
    $ids = [];
    $photo_ids = [];
    while ($page <= $total_pages) {
      $response = drupal_http_request($list_url . '&page=' . $page);
      if ($page === 1) {
        $per_page = $response->headers['x-per-page'];
        $total_entries = $response->headers['x-total-entries'];
        $total_pages = ceil($total_entries / $per_page);
      }
      $data = '';
      if ($response->data) {
        $data = drupal_json_decode($response->data);
        if ($data !== NULL) {
          $ids = array_merge($ids, $this->trimIdsFromJson($data));
          $photo_ids = array_merge($photo_ids, $this->managePhotos($data));
        }
      }
      if (!$data) {
        Migration::displayMessage(t('Loading of !listurl failed:', ['!listurl' => $list_url . '&page=' . $page]));
      }
      // As explained in the doc, https://api.inaturalist.org/v1/docs/
      // we can't make more than 100 requests per minute. To respect the limit,
      // we wait 0.9sec between each request. (near 67/sec)
      usleep(900000);
      $page++;
    }

    $json_ids = drupal_json_encode($ids);
    $photo_json_ids = drupal_json_encode($photo_ids);
    file_unmanaged_save_data($json_ids, $this->observationListFile, FILE_EXISTS_REPLACE);
    file_unmanaged_save_data($photo_json_ids, $this->photoListFile, FILE_EXISTS_REPLACE);

  }

  /**
   * Manage observation photos.
   */
  protected function managePhotos(array $data) {
    $photo_ids = [];

    foreach ($data as $item) {
      if (isset($item['photos'])) {
        foreach ($item['photos'] as $photo) {
          // Only import licensed photos, skip "all rights reserved".
          if (isset($photo['license']) && $photo['license'] !== 0) {
            $photo_ids[] = $photo['id'];
            $photo['observation_id'] = $item['id'];
            $photo_json = drupal_json_encode($photo);
            $photo_filename = $this->photoDirectory . '/' . $photo['id'] . '.json';
            file_unmanaged_save_data($photo_json, $photo_filename, FILE_EXISTS_REPLACE);
          }
        }
      }
    }

    return $photo_ids;
  }

  /**
   * Trim data from IDs list.
   */
  protected function trimIdsFromJson(array $data) {
    $ids = [];

    foreach ($data as $item) {
      if (isset($item['id'])) {
        $ids[]['id'] = $item['id'];
      }
    }

    return $ids;
  }

}
