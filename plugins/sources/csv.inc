<?php

/**
 * {@inheritdoc}
 */
class BanabiomapMigrateSourceCSV extends MigrateSourceCSV {

  /**
   * {@inheritdoc}
   */
  public function __construct($path, array $csvcolumns = array(), array $options = array(), array $fields = array()) {
    parent::__construct($path, $csvcolumns, $options, $fields);

    $dbxref_file = dirname($path) . '/dbxref.csv';
    if (!file_exists($dbxref_file)) {
      $this->generateChildFile($path, $dbxref_file, 23);
    }
  }

  /**
   * Generate a child file from an original file.
   *
   * @param string $parent_file
   *   Source file path.
   * @param string $child_file
   *   Child file path, to be generated.
   * @param int $id_pos
   *   ID column position in CSV file.
   */
  protected function generateChildFile($parent_file, $child_file, $id_pos) {
    $child_ids = [];
    $delimiter = $this->fgetcsv['delimiter'];
    $parent_handle = fopen($parent_file, 'r');
    if (!$parent_handle) {
      Migration::displayMessage(t('Could not open CSV file !url', ['!url' => $parent_file]));
      return;
    }
    $child_handle = fopen($child_file, 'w');
    if (!$child_handle) {
      Migration::displayMessage(t('Could not open CSV file !url', ['!url' => $child_file]));
      fclose($parent_handle);
      return;
    }
    while (($line = fgetcsv($parent_handle, 0, $delimiter)) !== FALSE) {
      if (!isset($line[$id_pos]) || empty($line[$id_pos])) {
        continue;
      }
      $child_id = $line[$id_pos];
      if (in_array($child_id, $child_ids)) {
        continue;
      }
      fputcsv($child_handle, $line, $delimiter);
      $child_ids[] = $child_id;
    }
    fclose($parent_handle);
    fclose($child_handle);
  }

}
