<?php

/**
 * @file
 * Additional properties for entities (like nodes mapping Chado data).
 */

/**
 * Getter callback for contributor_login property.
 */
function mgis_inaturalist_contributor_login_getter_callback($node, $options, $name, $type, $info) {
  if ($node->type != 'chado_stock' || !$node->status || !is_object($node->stock)) {
    return;
  }

  $stockprop = chado_get_property(['table' => 'stock', 'id' => $node->stock->stock_id], ['type_id' => MGIS_INATURALIST_CVTERM_INATURALIST_LOGIN]);

  if (!$stockprop) {
    return;
  }

  if (is_array($stockprop)) {
    $stockprop = current($stockprop);
  }

  return $stockprop->value;
}

/**
 * Getter callback for iNaturalist country property.
 */
function mgis_inaturalist_country_getter_callback($node, $options, $name, $type, $info) {
  if ($node->type != 'chado_stock' || !$node->status || !is_object($node->stock)) {
    return;
  }

  $stockprop = chado_get_property(['table' => 'stock', 'id' => $node->stock->stock_id], ['type_id' => MGIS_INATURALIST_CVTERM_INATURALIST_COUNTRY]);

  if (!$stockprop) {
    return;
  }

  if (is_array($stockprop)) {
    $stockprop = current($stockprop);
  }

  $country_a2_code = mgis_country_a3_to_a2($stockprop->value);
  if ($country_name = mgis_country_a2_to_name($country_a2_code)) {
    return $country_name;
  }
}

/**
 * Getter callback for iNaturalist observation date property.
 */
function mgis_inaturalist_observed_on_getter_callback($node, $options, $name, $type, $info) {
  if ($node->type != 'chado_stock' || !$node->status || !is_object($node->stock)) {
    return;
  }

  $sql = 'SELECT p.value
          FROM {nd_experimentprop} p
          JOIN {nd_experiment_stock} es USING (nd_experiment_id)
          WHERE es.stock_id = :stock_id AND p.type_id = :type_id
          ORDER BY p.value DESC
          LIMIT 1';
  $args = [
    ':stock_id' => $node->stock->stock_id,
    ':type_id' => MGIS_INATURALIST_CVTERM_OBSERVED_ON,
  ];

  if ($result = chado_query($sql, $args)->fetchField()) {
    return strtotime($result) ?: 0;
  }
}

/**
 * Getter callback for iNaturalist reviewed property.
 */
function mgis_inaturalist_observation_reviewed_getter_callback($node, $options, $name, $type, $info) {
  if ($node->type != 'chado_stock' || !$node->status || !is_object($node->stock)) {
    return;
  }

  $stock_cvterms = chado_select_record('stock_cvterm', ['is_not'], ['stock_id' => $node->stock->stock_id, 'cvterm_id' => MGIS_INATURALIST_CVTERM_REVIEWED]);
  if ($stock_cvterms) {
    $stock_cvterm = current($stock_cvterms);
    return !$stock_cvterm->is_not;
  }
}

/**
 * Getter callback for iNaturalist hidden property.
 */
function mgis_inaturalist_observation_hidden_getter_callback($node, $options, $name, $type, $info) {
  if ($node->type != 'chado_stock' || !$node->status || !is_object($node->stock)) {
    return;
  }

  $stock_cvterms = chado_select_record('stock_cvterm', ['is_not'], ['stock_id' => $node->stock->stock_id, 'cvterm_id' => MGIS_INATURALIST_CVTERM_HIDDEN]);
  if ($stock_cvterms) {
    $stock_cvterm = current($stock_cvterms);
    return !$stock_cvterm->is_not;
  }
}
