<?php

/**
 * @file
 * Declare migrations.
 */

/**
 * Implements hook_migrate_api().
 */
function mgis_inaturalist_migrate_api() {
  return [
    'api' => 2,
    'groups' => [
      'mgis_inaturalist' => [
        'title' => t('iNaturalist migrations'),
      ],
      'mgis_banabiomap' => [
        'title' => t('BaNaBioMap migrations'),
      ],
      'mgis_gbif' => [
        'title' => t('Migrations from GBIF'),
      ],
    ],
    'migrations' => [
      // iNaturalist.
      'inaturalist_pictures' => [
        'description' => t('Migration des photos'),
        'class_name' => 'MigrateMgisPictureMigration',
        'group_name' => 'mgis_inaturalist',
      ],
      'inaturalist_dbxref_picture' => [
        'description' => t("Création d'un dbxref pour chaque photo"),
        'class_name' => 'MigrateMgisDbxrefPicture',
        'group_name' => 'mgis_inaturalist',
        'dependencies' => ['inaturalist_pictures'],
      ],
      'inaturalist_dbxrefprop_picture_attribution' => [
        'description' => t('Attribution photos'),
        'class_name' => 'MigrateMgisDbxrefPropAttribution',
        'group_name' => 'mgis_inaturalist',
        'dependencies' => ['inaturalist_dbxref_picture'],
      ],
      'inaturalist_taxonomy' => [
        'description' => t('Migration de la taxonomie utilisée sur iNaturalist'),
        'class_name' => 'MigrateMgisTaxonomyMigration',
        'group_name' => 'mgis_inaturalist',
      ],
      'inaturalist_dbxref_cvterm_observation_source' => [
        'description' => t("Création d'un dbxref pour chaque cvterm « observation source »"),
        'class_name' => 'MigrateMgisDbxrefCvTermObservationSourceMigration',
        'group_name' => 'mgis_inaturalist',
      ],
      'inaturalist_dbxref_cvterm_plant_condition' => [
        'description' => t("Création d'un dbxref pour chaque cvterm « plant condition »"),
        'class_name' => 'MigrateMgisDbxrefCvTermPlantConditionMigration',
        'group_name' => 'mgis_inaturalist',
      ],
      'inaturalist_cvterm_observation_source' => [
        'description' => t("Migration des sources d'observation utilisées sur iNaturalist"),
        'class_name' => 'MigrateMgisCvtermObservationSourceMigration',
        'group_name' => 'mgis_inaturalist',
        'dependencies' => ['inaturalist_dbxref_cvterm_observation_source'],
      ],
      'inaturalist_cvterm_plant_condition' => [
        'description' => t("Migration des termes utilisés sur iNaturalist pour décrire l'état des individus"),
        'class_name' => 'MigrateMgisCvtermPlantConditionMigration',
        'group_name' => 'mgis_inaturalist',
        'dependencies' => ['inaturalist_dbxref_cvterm_plant_condition'],
      ],
      'inaturalist_stock' => [
        'description' => t('Migration des observations (stock) depuis iNaturalist'),
        'class_name' => 'MigrateMgisStockMigration',
        'group_name' => 'mgis_inaturalist',
        'dependencies' => ['inaturalist_taxonomy'],
      ],
      'inaturalist_dbxref_stock' => [
        'description' => t("Création d'un dbxref pour chaque observation iNaturalist"),
        'class_name' => 'MigrateMgisDbxrefStockMigration',
        'group_name' => 'mgis_inaturalist',
      ],
      'inaturalist_stock_dbxref' => [
        'description' => t('Association entre stock et dbxref précédemment créé'),
        'class_name' => 'MigrateMgisStockDbxrefMigration',
        'group_name' => 'mgis_inaturalist',
        'dependencies' => ['inaturalist_stock', 'inaturalist_dbxref_stock'],
      ],
      'inaturalist_stock_dbxref_picture' => [
        'description' => t('Association entre stock et dbxref des images précédemment créé'),
        'class_name' => 'MigrateMgisStockDbxrefPictureMigration',
        'group_name' => 'mgis_inaturalist',
        'dependencies' => ['inaturalist_dbxref_picture', 'inaturalist_stock'],
      ],
      'inaturalist_stock_cvterm_observation_source' => [
        'description' => t('Migration des liens entre cvterms « observation source » et stock'),
        'class_name' => 'MigrateMgisStockCvtermObservationSourceMigration',
        'group_name' => 'mgis_inaturalist',
        'dependencies' => [
          'inaturalist_cvterm_observation_source',
          'inaturalist_stock',
        ],
      ],
      'inaturalist_stock_cvterm_plant_condition' => [
        'description' => t('Migration des liens entre cvterms « plant condition » et stock'),
        'class_name' => 'MigrateMgisStockCvtermPlantConditionMigration',
        'group_name' => 'mgis_inaturalist',
        'dependencies' => [
          'inaturalist_cvterm_plant_condition',
          'inaturalist_stock',
        ],
      ],
      'inaturalist_stock_cvterm_observation_status_reviewed' => [
        'description' => t('Migration des liens entre cvterms « observation status » et stock (reviewed)'),
        'class_name' => 'MigrateMgisStockCvtermObservationStatusReviewedMigration',
        'group_name' => 'mgis_inaturalist',
        'dependencies' => ['inaturalist_stock'],
      ],
      'inaturalist_stock_cvterm_observation_status_hidden' => [
        'description' => t('Migration des liens entre cvterms « observation status » et stock (hidden)'),
        'class_name' => 'MigrateMgisStockCvtermObservationStatusHiddenMigration',
        'group_name' => 'mgis_inaturalist',
        'dependencies' => ['inaturalist_stock'],
      ],
      'inaturalist_stock_cvterm_active' => [
        'description' => t('Donner le statut « active » aux observations'),
        'class_name' => 'MigrateMgisStockCvtermActiveMigration',
        'group_name' => 'mgis_inaturalist',
        'dependencies' => ['inaturalist_stock'],
      ],
      'inaturalist_stockprop_additional_details' => [
        'description' => t('Migration « additional details »'),
        'class_name' => 'MigrateMgisStockPropAdditionalDetails',
        'group_name' => 'mgis_inaturalist',
        'dependencies' => ['inaturalist_stock'],
      ],
      'inaturalist_stockprop_user_login' => [
        'description' => t('Migration login utilisateur iNaturalist'),
        'class_name' => 'MigrateMgisStockPropUserLogin',
        'group_name' => 'mgis_inaturalist',
        'dependencies' => ['inaturalist_stock'],
      ],
      'inaturalist_stockprop_country' => [
        'description' => t("Migration du pays de l'observation"),
        'class_name' => 'MigrateMgisStockPropCountry',
        'group_name' => 'mgis_inaturalist',
        'dependencies' => ['inaturalist_stock'],
      ],
      'inaturalist_geolocation' => [
        'description' => t('Migration des géolocalisations des observations sur iNaturalist'),
        'class_name' => 'MigrateMgisGeolocationMigration',
        'group_name' => 'mgis_inaturalist',
      ],
      'inaturalist_experiment' => [
        'description' => t('Migration des observations (experiment) depuis iNaturalist'),
        'class_name' => 'MigrateMgisExperimentMigration',
        'group_name' => 'mgis_inaturalist',
        'dependencies' => ['inaturalist_geolocation'],
      ],
      'inaturalist_experiment_stock' => [
        'description' => t('Lien entre les observations (experiment) et le stock'),
        'class_name' => 'MigrateMgisExperimentStockMigration',
        'group_name' => 'mgis_inaturalist',
        'dependencies' => ['inaturalist_experiment', 'inaturalist_stock'],
      ],
      'inaturalist_experimentprop_observed_on' => [
        'description' => t('Migration des dates des observations'),
        'class_name' => 'MigrateMgisExperimentPropObservedOn',
        'group_name' => 'mgis_inaturalist',
        'dependencies' => ['inaturalist_experiment'],
      ],
      // BaNaBioMap.
      'banabiomap_taxonomy' => [
        'description' => t('Migration de la taxonomie utilisée pour BaNaBioMap'),
        'class_name' => 'MigrateBanabiomapTaxonomyMigration',
        'group_name' => 'mgis_banabiomap',
      ],
      'banabiomap_stock' => [
        'description' => t('Migration des observations (stock) pour BaNaBioMap'),
        'class_name' => 'MigrateBanabiomapStockMigration',
        'group_name' => 'mgis_banabiomap',
        'dependencies' => ['banabiomap_taxonomy'],
      ],
      'banabiomap_geolocation' => [
        'description' => t('Migration des géolocalisations des observations pour BaNaBioMap'),
        'class_name' => 'MigrateBanabiomapGeolocationMigration',
        'group_name' => 'mgis_banabiomap',
      ],
      'banabiomap_experiment' => [
        'description' => t('Migration des observations (experiment) pour BaNaBioMap'),
        'class_name' => 'MigrateBanabiomapExperimentMigration',
        'group_name' => 'mgis_banabiomap',
        'dependencies' => ['banabiomap_geolocation'],
      ],
      'banabiomap_experiment_stock' => [
        'description' => t('Lien entre les observations (experiment) et le stock'),
        'class_name' => 'MigrateBanabiomapExperimentStockMigration',
        'group_name' => 'mgis_banabiomap',
        'dependencies' => ['banabiomap_experiment', 'banabiomap_stock'],
      ],
      'banabiomap_experimentprop_observed_on' => [
        'description' => t('Migration des dates des observations'),
        'class_name' => 'MigrateBanabiomapExperimentPropObservedOn',
        'group_name' => 'mgis_banabiomap',
        'dependencies' => ['banabiomap_experiment'],
      ],
      'banabiomap_stockprop_country' => [
        'description' => t("Migration du pays de l'observation"),
        'class_name' => 'MigrateBanabiomapStockPropCountry',
        'group_name' => 'mgis_banabiomap',
        'dependencies' => ['banabiomap_stock'],
      ],
      'banabiomap_stockprop_province' => [
        'description' => t("Migration de l'État, province ou district de l'observation"),
        'class_name' => 'MigrateBanabiomapStockPropProvince',
        'group_name' => 'mgis_banabiomap',
        'dependencies' => ['banabiomap_stock'],
      ],
      'banabiomap_stockprop_city' => [
        'description' => t("Migration de la commune de l'observation"),
        'class_name' => 'MigrateBanabiomapStockPropCity',
        'group_name' => 'mgis_banabiomap',
        'dependencies' => ['banabiomap_stock'],
      ],
      'banabiomap_dbxref_stock' => [
        'description' => t("Création d'un dbxref pour chaque observation de BaNaBioMap"),
        'class_name' => 'MigrateBanabiomapDbxrefStockMigration',
        'group_name' => 'mgis_banabiomap',
      ],
      'banabiomap_stock_dbxref' => [
        'description' => t('Association entre stock et dbxref précédemment créé'),
        'class_name' => 'MigrateBanabiomapStockDbxrefMigration',
        'group_name' => 'mgis_banabiomap',
        'dependencies' => ['banabiomap_stock', 'banabiomap_dbxref_stock'],
      ],
      'banabiomap_stock_cvterm_observation_status_reviewed' => [
        'description' => t('Migration des liens entre cvterms « observation status » et stock (reviewed)'),
        'class_name' => 'MigrateBanabiomapStockCvtermObservationStatusReviewedMigration',
        'group_name' => 'mgis_banabiomap',
        'dependencies' => ['banabiomap_stock'],
      ],
      'banabiomap_stock_cvterm_observation_status_hidden' => [
        'description' => t('Migration des liens entre cvterms « observation status » et stock (hidden)'),
        'class_name' => 'MigrateBanabiomapStockCvtermObservationStatusHiddenMigration',
        'group_name' => 'mgis_banabiomap',
        'dependencies' => ['banabiomap_stock'],
      ],
      'banabiomap_stock_cvterm_active' => [
        'description' => t('Donner le statut « active » aux observations'),
        'class_name' => 'MigrateBanabiomapStockCvtermActiveMigration',
        'group_name' => 'mgis_banabiomap',
        'dependencies' => ['banabiomap_stock'],
      ],
      'banabiomap_pictures' => [
        'description' => t('Migration des photos'),
        'class_name' => 'MigrateBanabiomapPictureMigration',
        'group_name' => 'mgis_banabiomap',
      ],
      'banabiomap_dbxref_picture' => [
        'description' => t("Création d'un dbxref pour chaque photo"),
        'class_name' => 'MigrateBanabiomapDbxrefPicture',
        'group_name' => 'mgis_banabiomap',
        'dependencies' => ['banabiomap_pictures'],
      ],
      'banabiomap_dbxrefprop_picture_attribution' => [
        'description' => t('Attribution photos'),
        'class_name' => 'MigrateBanabiomapDbxrefPropAttribution',
        'group_name' => 'mgis_banabiomap',
        'dependencies' => ['banabiomap_dbxref_picture'],
      ],
      'banabiomap_stock_dbxref_picture' => [
        'description' => t('Association entre stock et dbxref des images précédemment créé'),
        'class_name' => 'MigrateBanabiomapStockDbxrefPictureMigration',
        'group_name' => 'mgis_banabiomap',
        'dependencies' => ['banabiomap_dbxref_picture', 'banabiomap_stock'],
      ],
      'banabiomap_stockprop_user_login' => [
        'description' => t('Migration login utilisateur BaNaBioMap'),
        'class_name' => 'MigrateBanabiomapStockPropUserLogin',
        'group_name' => 'mgis_banabiomap',
        'dependencies' => ['banabiomap_stock'],
      ],
      'banabiomap_stockprop_uncertainty' => [
        'description' => t('Migration incertitude localisation BaNaBioMap'),
        'class_name' => 'MigrateBanabiomapStockPropUncertainty',
        'group_name' => 'mgis_banabiomap',
        'dependencies' => ['banabiomap_stock'],
      ],
      'banabiomap_stockprop_source_name' => [
        'description' => t('Migration nom source BaNaBioMap'),
        'class_name' => 'MigrateBanabiomapStockPropSourceName',
        'group_name' => 'mgis_banabiomap',
        'dependencies' => ['banabiomap_stock'],
      ],
      'banabiomap_stockprop_source_type' => [
        'description' => t('Migration type source BaNaBioMap'),
        'class_name' => 'MigrateBanabiomapStockPropSourceType',
        'group_name' => 'mgis_banabiomap',
        'dependencies' => ['banabiomap_stock'],
      ],
      'banabiomap_stockprop_note' => [
        'description' => t('Migration remarques BaNaBioMap'),
        'class_name' => 'MigrateBanabiomapStockPropNote',
        'group_name' => 'mgis_banabiomap',
        'dependencies' => ['banabiomap_stock'],
      ],
      'banabiomap_stockprop_usability' => [
        'description' => t('Migration utilisabilité BaNaBioMap'),
        'class_name' => 'MigrateBanabiomapStockPropUsability',
        'group_name' => 'mgis_banabiomap',
        'dependencies' => ['banabiomap_stock'],
      ],
      // GBIF.
      'gbif_taxonomy' => [
        'description' => t("Migration de la taxonomie utilisée pour l'importation depuis le GBIF"),
        'class_name' => 'MigrateGbifTaxonomyMigration',
        'group_name' => 'mgis_gbif',
      ],
      'gbif_stock' => [
        'description' => t('Migration des observations (stock) depuis le GBIF'),
        'class_name' => 'MigrateGbifStockMigration',
        'group_name' => 'mgis_gbif',
        'dependencies' => ['gbif_taxonomy'],
      ],
      'gbif_geolocation' => [
        'description' => t('Migration des géolocalisations des observations provenant du GBIF'),
        'class_name' => 'MigrateGbifGeolocationMigration',
        'group_name' => 'mgis_gbif',
      ],
      'gbif_experiment' => [
        'description' => t('Migration des observations (experiment) provenant du GBIF'),
        'class_name' => 'MigrateGbifExperimentMigration',
        'group_name' => 'mgis_gbif',
        'dependencies' => ['gbif_geolocation'],
      ],
      'gbif_experiment_stock' => [
        'description' => t('Lien entre les observations (experiment) et le stock'),
        'class_name' => 'MigrateGbifExperimentStockMigration',
        'group_name' => 'mgis_gbif',
        'dependencies' => ['gbif_experiment', 'gbif_stock'],
      ],
      'gbif_experimentprop_observed_on' => [
        'description' => t('Migration des dates des observations'),
        'class_name' => 'MigrateGbifExperimentPropObservedOn',
        'group_name' => 'mgis_gbif',
        'dependencies' => ['gbif_experiment'],
      ],
      'gbif_stockprop_country' => [
        'description' => t("Migration du pays de l'observation"),
        'class_name' => 'MigrateGbifStockPropCountry',
        'group_name' => 'mgis_gbif',
        'dependencies' => ['gbif_stock'],
      ],
      'gbif_stockprop_province' => [
        'description' => t("Migration de l'État, province ou district de l'observation"),
        'class_name' => 'MigrateGbifStockPropProvince',
        'group_name' => 'mgis_gbif',
        'dependencies' => ['gbif_stock'],
      ],
      'gbif_stockprop_county' => [
        'description' => t("Migration du département de l'observation"),
        'class_name' => 'MigrateGbifStockPropCounty',
        'group_name' => 'mgis_gbif',
        'dependencies' => ['gbif_stock'],
      ],
      'gbif_dbxref_stock' => [
        'description' => t("Création d'un dbxref pour chaque observation provenant du GBIF"),
        'class_name' => 'MigrateGbifDbxrefStockMigration',
        'group_name' => 'mgis_gbif',
      ],
      'gbif_stock_dbxref' => [
        'description' => t('Association entre stock et dbxref précédemment créé'),
        'class_name' => 'MigrateGbifStockDbxrefMigration',
        'group_name' => 'mgis_gbif',
        'dependencies' => ['gbif_stock', 'gbif_dbxref_stock'],
      ],
      'gbif_stock_cvterm_observation_status_reviewed' => [
        'description' => t('Migration des liens entre cvterms « observation status » et stock (reviewed)'),
        'class_name' => 'MigrateGbifStockCvtermObservationStatusReviewedMigration',
        'group_name' => 'mgis_gbif',
        'dependencies' => ['gbif_stock'],
      ],
      'gbif_stock_cvterm_observation_status_hidden' => [
        'description' => t('Migration des liens entre cvterms « observation status » et stock (hidden)'),
        'class_name' => 'MigrateGbifStockCvtermObservationStatusHiddenMigration',
        'group_name' => 'mgis_gbif',
        'dependencies' => ['gbif_stock'],
      ],
      'gbif_stock_cvterm_active' => [
        'description' => t('Donner le statut « active » aux observations'),
        'class_name' => 'MigrateGbifStockCvtermActiveMigration',
        'group_name' => 'mgis_gbif',
        'dependencies' => ['gbif_stock'],
      ],
      'gbif_stockprop_user_login' => [
        'description' => t('Migration login utilisateur GBIF'),
        'class_name' => 'MigrateGbifStockPropUserLogin',
        'group_name' => 'mgis_gbif',
        'dependencies' => ['gbif_stock'],
      ],
      'gbif_stockprop_uncertainty' => [
        'description' => t('Migration incertitude localisation GBIF'),
        'class_name' => 'MigrateGbifStockPropUncertainty',
        'group_name' => 'mgis_gbif',
        'dependencies' => ['gbif_stock'],
      ],
      'gbif_stockprop_note' => [
        'description' => t('Migration remarques BaNaBioMap'),
        'class_name' => 'MigrateGbifStockPropNote',
        'group_name' => 'mgis_gbif',
        'dependencies' => ['gbif_stock'],
      ],
      'gbif_pictures' => [
        'description' => t('Migration des photos'),
        'class_name' => 'MigrateGbifPictureMigration',
        'group_name' => 'mgis_gbif',
      ],
      'gbif_dbxref_picture' => [
        'description' => t("Création d'un dbxref pour chaque photo"),
        'class_name' => 'MigrateGbifDbxrefPicture',
        'group_name' => 'mgis_gbif',
        'dependencies' => ['gbif_pictures'],
      ],
      'gbif_dbxrefprop_picture_attribution' => [
        'description' => t('Attribution photos'),
        'class_name' => 'MigrateGbifDbxrefPropAttribution',
        'group_name' => 'mgis_gbif',
        'dependencies' => ['gbif_dbxref_picture'],
      ],
      'gbif_stock_dbxref_picture' => [
        'description' => t('Association entre stock et dbxref des images précédemment créé'),
        'class_name' => 'MigrateGbifStockDbxrefPictureMigration',
        'group_name' => 'mgis_gbif',
        'dependencies' => ['gbif_dbxref_picture', 'gbif_stock'],
      ],
    ],
  ];
}
