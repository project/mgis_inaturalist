<?php

/**
 * Special rendering for organism_ids.
 */
class views_handler_field_organism_id extends views_handler_field_numeric {

  /**
   * {@inheritdoc}
   */
  public function option_definition() {
    $options = parent::option_definition();

    $options['format'] = ['default' => '', 'translatable' => TRUE];

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $form['format'] = array(
      '#title' => t('Format'),
      '#options' => [
        '' => t('- Use default -'),
        'genus' => t('Genus'),
        'species' => t('Species/group'),
        'subspecies' => t('Subspecies/subgroup'),
      ],
      '#type' => 'select',
      '#default_value' => $this->options['format'],
      '#description' => t('How to format this organism.'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function render($values) {
    if (empty($this->options['format'])) {
      return parent::render($values);
    }

    $value = $this->get_value($values);
    $callback = 'render_' . $this->options['format'];
    $key = $callback . '_' . $value;
    $render = &drupal_static($key);

    if (!isset($render)) {
      if ($cache = cache_get($key)) {
        $render = $cache->data;
      }
      else {
        $render = call_user_func([$this, $callback], $value);
        cache_set($key, $render, 'cache');
      }
    }

    return $render;
  }

  /**
   * Format as genus.
   */
  protected function render_genus($value) {
    return mgis_get_accession_taxonomy([
      'organism_id' => $value,
      'selectors' => ['genus'],
      'empty' => '-',
    ]);
  }

  /**
   * Format as species/group.
   */
  protected function render_species($value) {
    return mgis_get_accession_taxonomy([
      'organism_id' => $value,
      'selectors' => ['species', 'group'],
      'empty' => '-',
    ]);
  }

  /**
   * Format as subspecies/subgroup.
   */
  protected function render_subspecies($value) {
    return mgis_get_accession_taxonomy([
      'organism_id' => $value,
      'selectors' => ['subspecies', 'subgroup'],
      'empty' => '-',
    ]);
  }

}
