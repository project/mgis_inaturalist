<?php

/**
 * GBIF import status.
 */
class views_handler_gbif_field_import_status extends views_handler_field {

  /**
   * Local status of GBIF occurrences.
   *
   * @var array
   */
  protected $statuses = [];

  /**
   * {@inheritdoc}
   */
  public function query() {
  }

  /**
   * {@inheritdoc}
   */
  public function pre_render(&$values) {
    // Extract GBIF keys (IDs) from current results.
    $keys = [];
    foreach ($values as $value) {
      $keys[] = $value->key;
    }
    // Find associated statuses.
    if ($keys) {
      $this->statuses = db_select('gbif_import', 'gi')
        ->fields('gi', ['key', 'status'])
        ->condition('key', $keys)
        ->execute()
        ->fetchAllKeyed();
    }

    // Replace numeric values by labels.
    $labels = mgis_inaturalist_gbif_import_status_value();
    foreach ($this->statuses as &$status) {
      $status = $labels[$status];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function get_value($values, $field = NULL) {
    $key = $values->{$this->view->base_field};
    return $this->statuses[$key] ?? t('N/A');
  }

}
