<?php

/**
 * Insert a link to export all in situ observations inside of an area.
 */
class views_handler_area_in_situ_csv extends views_handler_area {

  /**
   * {@inheritdoc}
   */
  public function render($empty = FALSE) {
    if (user_access('export in situ observations')) {
      return l(t('Export all in situ observations'), 'in-situ-observations.csv');
    }

    return '';
  }

}
