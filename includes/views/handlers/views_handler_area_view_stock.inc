<?php

/**
 * Insert a view inside of an area and pass stock ids as argument.
 */
class views_handler_area_view_stock extends views_handler_area_view {

  /**
   * {@inheritdoc}
   */
  public function render($empty = FALSE) {
    if ($view = $this->loadView()) {
      $stock_ids = array_map(function ($result) {
        return $result->_entity_properties['stock_id'];
      }, $this->view->result);
      $stock_ids = implode('+', $stock_ids);
      return $view->preview(NULL, [$stock_ids]);
    }
    return '';
  }

}
