<?php

/**
 * Insert a map legend for Musa in situ.
 */
class views_handler_area_map_legend extends views_handler_area {

  /**
   * {@inheritdoc}
   */
  public function render($empty = FALSE) {
    $organisms = mgis_inaturalist_organism_color($this->view->result);
    $path = ip_geoloc_marker_directory();

    $legend = '<div class="map-legend">';

    foreach ($organisms['organisms'] as $organism_id => $value) {
      $legend .= '<span class="legend-item">';
      $legend .= theme('image', [
        'path' => $path . '/' . $value['color'] . '.png',
        'width' => 16,
        'height' => 21,
        'alt' => $value['color'],
      ]);
      $legend .= t('<span class="label">@label (@count)</span>', [
        '@label' => $value['label'],
        '@count' => $value['count'],
      ]);
      $legend .= '</span>';
    }
    if ($organisms['other']) {
      $legend .= '<span class="legend-item">';
      $legend .= theme('image', [
        'path' => libraries_get_path('leaflet') . '/images/marker-icon.png',
        'width' => 16,
        'height' => 21,
        'alt' => t('Other'),
      ]);
      $legend .= t('<span class="label">Other</span>');
      $legend .= '</span>';
    }

    $legend .= '</div>';

    return $legend;
  }

}
