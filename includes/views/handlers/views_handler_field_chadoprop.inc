<?php

/**
 * Special rendering for Chado properties.
 */
class views_handler_field_chadoprop extends views_handler_field {

  /**
   * {@inheritdoc}
   */
  public function option_definition() {
    $options = parent::option_definition();

    $options['format'] = ['default' => '', 'translatable' => TRUE];

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $form['format'] = array(
      '#title' => t('Format'),
      '#options' => [
        '' => t('- Use default -'),
        'country' => t('Country name (from A3 code)'),
      ],
      '#type' => 'select',
      '#default_value' => $this->options['format'],
      '#description' => t('How to format this property.'),
    );

  }

  /**
   * {@inheritdoc}
   */
  public function render($values) {
    if (empty($this->options['format'])) {
      return parent::render($values);
    }
    $value = $this->get_value($values);
    return call_user_func([$this, 'render_' . $this->options['format']], $value);
  }

  /**
   * Format a country name.
   */
  protected function render_country($value) {
    $country_a2_code = mgis_country_a3_to_a2($value);
    if ($country_name = mgis_country_a2_to_name($country_a2_code)) {
      return $country_name;
    }
    return t('Unknown');
  }

}
