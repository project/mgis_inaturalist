<?php

/**
 * Override ip_geoloc_plugin_style plugin.
 *
 * Dynamically colorize accessions on map.
 */
class mgis_inaturalist_plugin_style_leaflet extends ip_geoloc_plugin_style_leaflet {

  /**
   * {@inheritdoc}
   */
  public function render() {
    if (empty($this->options['map']) || !($map = ip_geoloc_plugin_style_leaflet_map_get_info($this->options['map']))) {
      return t('No Leaflet map was selected or map configuration was not found.');
    }
    if (!empty($this->view->live_preview)) {
      return t('The preview function is incompatible with Leaflet maps so cannot be used. Please visit the page path or the block to view your map.');
    }
    $render_start = microtime(TRUE);

    ip_geoloc_plugin_style_render_fields($this);

    $open_balloons_on_click = !empty($this->options['open_balloons_on_click']);
    $open_balloons_on_hover = !empty($this->options['open_balloons_on_hover']);

    $enable_balloons = $open_balloons_on_click || $open_balloons_on_hover;
    $locations = $this->extractLocations();

    $this->fill_out_location_regions($locations);

    $marker_color = $this->options['default_marker']['default_marker_color'];
    $visitor_marker_color = $this->options['visitor_marker']['visitor_marker_color'];
    $center_option = !isset($this->options['center_option']) ? 0 : $this->options['center_option'];
    $sync_flags = 0;
    if (!empty($this->options['sync'][LEAFLET_SYNC_CONTENT_TO_MARKER])) {
      $sync_flags |= LEAFLET_SYNC_CONTENT_TO_MARKER;
    }
    if (!empty($this->options['sync'][LEAFLET_SYNC_MARKER_TO_CONTENT])) {
      $sync_flags |= LEAFLET_SYNC_MARKER_TO_CONTENT;
      if (!empty($this->options['sync'][LEAFLET_SYNC_MARKER_TO_CONTENT_WITH_POPUP])) {
        $sync_flags |= LEAFLET_SYNC_MARKER_TO_CONTENT_WITH_POPUP;
      }
      if (!empty($this->options['sync'][LEAFLET_SYNC_REVERT_LAST_MARKER_ON_MAP_OUT])) {
        $sync_flags |= LEAFLET_SYNC_REVERT_LAST_MARKER_ON_MAP_OUT;
      }
    }
    $has_full_screen = !empty($this->options['full_screen']);
    $has_mini_map = !empty($this->options['mini_map']['on']);
    $zoom_indicator = empty($this->options['zoom_indicator']) ? FALSE : TRUE;
    $scale_control = FALSE;
    if (!empty($this->options['scale_metric']) || !empty($this->options['scale_imperial'])) {
      $scale_control = array(
        'metric' => !empty($this->options['scale_metric']),
        'imperial' => !empty($this->options['scale_imperial']),
      );
    }
    $goto_content_on_click = !empty($this->options['goto_content_on_click']);
    $reset_control = FALSE;
    if (!empty($this->options['map_reset'])) {
      $label = filter_xss_admin($this->options['map_reset_css_class']);
      $reset_control = array('label' => empty($label) ? ' ' : $label);
    }
    $cluster_control = FALSE;
    if (module_exists('leaflet_markercluster') && !empty($this->options['map_cluster_toggle'])) {
      $cluster_control = array('label' => 'C');
    }
    $cluster_radius = (int) _ip_geoloc_get_session_value('markercluster-radius');
    if ($cluster_radius < 2) {
      $cluster_radius = (int) $this->options['cluster_radius'];
    }
    $disable_clustering_at_zoom = $this->options['disable_clustering_at_zoom'];
    $hull_hug_factor = empty($this->options['cluster_differentiator']['cluster_outline']) ? -1 : 'auto';
    $cluster_tooltips = !empty($this->options['cluster_differentiator']['cluster_tooltips']);
    $cluster_touch_mode = empty($this->options['cluster_differentiator']['cluster_touch_mode']) ? 0 : 'auto';
    $cluster_aggregation_field = $this->options['cluster_aggregation']['aggregation_field'];
    $cluster_aggregation_function = $this->options['cluster_aggregation']['aggregation_function'];
    $cluster_aggregate_ranges = $this->options['cluster_aggregation']['ranges'];
    $cluster_aggregate_precision = $this->options['cluster_aggregation']['precision'];
    $allow_clusters_of_one = !empty($this->options['allow_clusters_of_one']);
    $tag_css_classes = $this->options['tags']['tag_css_class'];
    $module_path = drupal_get_path('module', 'ip_geoloc');
    $marker_path = file_create_url(ip_geoloc_marker_directory());
    $max_zoom = (int) $this->options['map_options']['maxzoom'];
    $zoom = max(1, (int) $this->options['map_options']['zoom']);
    $zoom_on_click = (int) $this->options['map_options']['zoom_on_click'];
    $scroll_wheel_zoom = (bool) $this->options['map_options']['scrollwheelzoom'];
    $dragging = (bool) $this->options['map_options']['dragging'];

    $visitor_location = ip_geoloc_get_visitor_location();
    if (!isset($visitor_location['latitude'])) {
      $visitor_location = db_query('SELECT * FROM {ip_geoloc} WHERE ip_address = :ip_address', array(':ip_address' => ip_address()))->fetchAssoc();
    }
    $use_specified_center = !empty($this->options['map_options']['center_lat']) && !empty($this->options['map_options']['center_lon'])
    && empty($visitor_location['is_updated']);
    if ($use_specified_center) {
      $map['center'] = array(
        'lat' => $this->options['map_options']['center_lat'],
        'lon' => $this->options['map_options']['center_lon'],
      );
    }
    elseif (!empty($locations) &&
        ($center_option == IP_GEOLOC_MAP_CENTER_ON_FIRST_LOCATION ||
            ($visitor_marker_color == 'none' && count($locations) == 1))) {
      $map['center'] = _ip_geoloc_get_center(reset($locations));
    }
    elseif (($center_option == IP_GEOLOC_MAP_CENTER_OF_LOCATIONS || $center_option == IP_GEOLOC_MAP_CENTER_OF_LOCATIONS_WEIGHTED) && !empty($locations)) {
      list($center_lat, $center_lon) = ip_geoloc_center_of_locations($locations, $center_option == IP_GEOLOC_MAP_CENTER_OF_LOCATIONS_WEIGHTED);
      $map['center'] = array(
        'lat' => $center_lat,
        'lon' => $center_lon,
      );
    }
    if (!$use_specified_center && (empty($locations) || $center_option == IP_GEOLOC_MAP_CENTER_ON_VISITOR) && isset($visitor_location['latitude'])) {
      $map['center'] = array(
        'lat' => $visitor_location['latitude'],
        'lon' => $visitor_location['longitude'],
      );
    }

    if (empty($locations)) {
      $ll = trim($this->options['empty_map_center']);
      if (empty($ll)) {
        // No map whatsoever.
        return;
      }
      if ($ll != t('visitor')) {
        // Empty map centered on coordinates provided.
        list($map['center']['lat'], $map['center']['lon']) = preg_split("/[\s,]+/", $ll);
      }
      // else: empty map centered on visitor location, as set above.
    }
    else {
      uasort($locations, '_ip_geoloc_plugin_style_leaflet_compare');
    }

    $marker_dimensions = explode('x', ip_geoloc_marker_dimensions());
    $marker_width = (int) $marker_dimensions[0];
    $marker_height = (int) $marker_dimensions[1];

    switch (variable_get('ip_geoloc_marker_anchor_pos', 'bottom')) {
      case 'top':
        $marker_anchor = 0;
        break;

      case 'middle':
        $marker_anchor = (int) (($marker_height + 1) / 2);
        break;

      default:
        $marker_anchor = $marker_height;
    }

    $features = array();
    foreach ($locations as $location) {
      $feature = array();
      if (isset($location->latitude) || isset($location->lat)) {
        $feature['type'] = 'point';
        $feature['lat'] = isset($location->latitude) ? $location->latitude : $location->lat;
        $feature['lon'] = isset($location->longitude) ? $location->longitude : $location->lon;
        if (!empty($location->random_displacement)) {
          ip_geoloc_add_random_displacement($feature, $location->random_displacement);
          $circle = array(
            'type' => 'circle',
            'lat' => $feature['lat'],
            'lon' => $feature['lon'],
            'radius' => $location->random_displacement,
          );
          $features[] = $circle;
        }
      }
      elseif (isset($location->component)) {
        // Possibly parsed by leaflet_process_geofield()
        // see _ip_geoloc_plugin_style_extract_lat_lng().
        $feature['type'] = $location->type;
        $feature['component'] = $location->component;
      }
      elseif (isset($location->points)) {
        $feature['type'] = $location->type;
        $feature['points'] = $location->points;
      }

      if (isset($location->id)) {
        // Allow marker events to identify the corresponding node.
        $feature['feature_id'] = $location->id;
      }

      // At this point $feature['type'] should be set.
      if (!empty($feature['type']) && $feature['type'] != 'point') {
        // Linestring, polygon ...
        $feature['flags'] = LEAFLET_MARKERCLUSTER_EXCLUDE_FROM_CLUSTER;
      }
      elseif (!isset($feature['lat'])) {
        // Points must have coords.
        continue;
      }
      if (!empty($sync_flags)) {
        $feature['flags'] = isset($feature['flags']) ? $feature['flags'] | $sync_flags : $sync_flags;
      }
      if ($enable_balloons && isset($location->balloon_text)) {
        $feature['popup'] = $location->balloon_text;
      }
      if (!empty($location->marker_special_char) || !empty($location->marker_special_char_class)) {
        $has_special_markers = TRUE;
        if (!empty($location->marker_special_char)) {
          $feature['specialChar'] = filter_xss_admin($location->marker_special_char);
        }
        if (!empty($location->marker_special_char_class)) {
          $feature['specialCharClass'] = filter_xss_admin($location->marker_special_char_class);
        }
      }
      elseif (!empty($this->options['default_marker']['default_marker_special_char'])
        || !empty($this->options['default_marker']['default_marker_special_char_class'])) {
        $has_special_markers = TRUE;
        $feature['specialChar'] = $this->options['default_marker']['default_marker_special_char'];
        $feature['specialCharClass'] = $this->options['default_marker']['default_marker_special_char_class'];
      }
      if (!empty($location->marker_tooltip)) {
        if (module_exists('leaflet_label')) {
          $feature['label'] = $location->marker_tooltip;
        }
        else {
          $has_special_markers = TRUE;
          $feature['tooltip'] = $location->marker_tooltip;
        }
      }

      if (!empty($location->regions)) {
        $has_special_markers = TRUE;
        // Make sure we start with 0 or regions will come across as Object
        // rather than array.
        $feature['regions'] = array(0 => '') + $location->regions;

        if (isset($location->aggregation_value)) {
          $feature['aggregationValue'] = (float) $location->aggregation_value;
        }
        // Note: cannot use <br/>  or HTML in tooltip as separator. Use \n.
        $feature['tooltip'] = empty($feature['tooltip']) ? '' : $feature['tooltip'] . "\n";
        $second_last = count($feature['regions']) - 2;
        if ($second_last > 0) {
          $feature['tooltip'] .= $feature['regions'][$second_last] . ' - ';
        }
        $feature['tooltip'] .= end($feature['regions']);
      }
      if (!empty($location->marker_tag)) {
        $has_special_markers = TRUE;
        $feature['tag'] = $location->marker_tag;
      }
      if (!empty($tag_css_classes)) {
        $feature['cssClass'] = $tag_css_classes;
      }
      if ((isset($location->marker_color) && _ip_geoloc_is_no_marker($location->marker_color)) ||
        (!isset($location->marker_color) && _ip_geoloc_is_no_marker($marker_color))) {
        // "No marker" as opposed to "default" marker.
        $has_special_markers = TRUE;
        $feature['icon'] = FALSE;
      }
      elseif (!empty($location->marker_color) || !empty($marker_color)) {
        // Switch from default icon to specified color.
        $color = empty($location->marker_color) ? $marker_color : $location->marker_color;
        $feature['icon'] = array(
          'iconUrl' => $marker_path . "/$color.png",
          'iconSize' => array('x' => $marker_width, 'y' => $marker_height),
          'iconAnchor' => array('x' => (int) (($marker_width + 1) / 2), 'y' => $marker_anchor),
          // Just above topline, center.
          'popupAnchor' => array('x' => 0, 'y' => -$marker_height - 1),
        );
      }
      $features[] = $feature;
    }
    if (isset($visitor_location['latitude'])) {
      if ($visitor_marker_color != 'none') {
        // See leaflet/README.txt for examples of Leaflet "features".
        $visitor_feature = array(
          'type' => 'point',
          'lat' => $visitor_location['latitude'],
          'lon' => $visitor_location['longitude'],
          'specialChar' => filter_xss_admin($this->options['visitor_marker']['visitor_marker_special_char']),
          'specialCharClass' => filter_xss_admin($this->options['visitor_marker']['visitor_marker_special_char_class']),
          'popup' => !empty($visitor_location['popup']) ? $visitor_location['popup'] : t('Your approximate location'),
          'tooltip' => !empty($visitor_location['tooltip']) ? $visitor_location['tooltip'] : t('Your approximate location'),
          'zIndex' => 9999,
          // See leaflet_markercluster.drupal.js.
          'flags' => LEAFLET_MARKERCLUSTER_EXCLUDE_FROM_CLUSTER,
        );
        if ($visitor_marker_color != '') {
          if (!empty($visitor_feature['specialChar']) || !empty($visitor_feature['specialCharClass'])) {
            $has_special_markers = TRUE;
          }
          $visitor_feature['icon'] = array(
            'iconUrl' => $marker_path . "/$visitor_marker_color.png",
            'iconSize' => array('x' => $marker_width, 'y' => $marker_height),
            'iconAnchor' => array('x' => (int) (($marker_width + 1) / 2), 'y' => $marker_anchor),
            // Just above topline, center.
            'popupAnchor' => array('x' => 0, 'y' => -$marker_height - 1),
          );
        }
        $features[] = $visitor_feature;
      }
      if (!empty($this->options['visitor_marker']['visitor_marker_accuracy_circle']) && !empty($visitor_location['accuracy'])) {
        $visitor_accuracy_circle = array(
          'type' => 'circle',
          'lat' => $visitor_location['latitude'],
          'lon' => $visitor_location['longitude'],
          'radius' => (float) $visitor_location['accuracy'],
          'popup' => !empty($visitor_location['popup']) ? $visitor_location['popup'] : t("You are within @m meters of the centre of this circle.", array('@m' => $visitor_location['accuracy'])),
          'label' => !empty($visitor_location['tooltip']) ? $visitor_location['tooltip'] : t('You are within this circle'),
          'zIndex' => 9998,
          'flags' => LEAFLET_MARKERCLUSTER_EXCLUDE_FROM_CLUSTER,
        );
        $features[] = $visitor_accuracy_circle;
      }
    }
    // If auto-box is chosen ($center_option==0), zoom only when there are
    // 0 or 1 markers [#1863374].
    if (!$use_specified_center && empty($center_option) && count($features) > 1) {
      unset($map['center']);
    }
    else {
      $map['settings']['zoom'] = $zoom;
      if (!empty($map['center'])) {
        // A leaflet.drupal.js quirk? Have to specify AND force a center...
        $map['center']['force'] = TRUE;
      }
    }
    $map['settings']['maxZoom'] = $max_zoom;
    $map['settings']['scrollWheelZoom'] = $scroll_wheel_zoom;
    $map['settings']['dragging'] = $dragging;

    $map['settings']['revertLastMarkerOnMapOut'] =
    (bool) ($sync_flags & LEAFLET_SYNC_REVERT_LAST_MARKER_ON_MAP_OUT);

    $map['settings']['maxClusterRadius'] = 0;
    if ($cluster_radius > 0) {
      if (module_exists('leaflet_markercluster')) {
        $map['settings']['maxClusterRadius'] = $cluster_radius;
        $map['settings']['disableClusteringAtZoom'] = $disable_clustering_at_zoom;
        $map['settings']['addRegionToolTips'] = $cluster_tooltips;
        $map['settings']['hullHugFactor'] = $hull_hug_factor;
        $map['settings']['touchMode'] = $cluster_touch_mode;
        $map['settings']['animateAddingMarkers'] = TRUE;
        if (!empty($cluster_aggregation_field)) {
          $map['settings']['clusterAggregationFunction'] = $cluster_aggregation_function;
          $map['settings']['clusterAggregateRanges'] = $cluster_aggregate_ranges;
          $map['settings']['clusterAggregatePrecision'] = $cluster_aggregate_precision;
          drupal_add_css(leaflet_markercluster_get_library_path() . '/MarkerCluster.Aggregations.css');
        }
        if ($allow_clusters_of_one) {
          $map['settings']['allowClustersOfOne'] = TRUE;
          $map['settings']['spiderfyDistanceMultiplier'] = 4.0;
        }
      }
      else {
        $display_name = $this->view->get_human_name() . ' (' . $this->display->display_title . ')';
        drupal_set_message(t('Cannot cluster markers in View %display_name, as the module Leaflet MarkerCluster is not enabled.', array('%display_name' => $display_name)), 'warning');
      }
    }

    $zoom_ranges = array_filter($this->options['cluster_differentiator']['zoom_ranges']);
    if (!empty($zoom_ranges)) {
      // Make sure we start array with 0 and no missing elements. Otherwise this
      // array will arrive as an Object on the JS side.
      $region_levels = array_fill(0, $max_zoom + 1, 0);
      foreach ($zoom_ranges as $level => $zoom_range) {
        for ($zoom = 1; $zoom <= $max_zoom; $zoom++) {
          if (ip_geoloc_is_in_range($zoom, $zoom_range)) {
            $region_levels[$zoom] = $level;
          }
        }
      }
      // Remove any gaps and zeroes.
      for ($z = 1; $z <= $max_zoom; $z++) {
        if (empty($region_levels[$z])) {
          $region_levels[$z] = $region_levels[$z - 1];
        }
      }
      $map['settings']['regionLevels'] = $region_levels;
    }
    // See [#1802732].
    $map_id = 'ip-geoloc-map-of-view-' . $this->view->name . '-' . $this->display->id . '-' . md5(serialize($features));

    drupal_add_js(drupal_get_path('module', 'leaflet') . '/leaflet.drupal.js');

    // Don't load sync JS and CSS when option is not requested.
    if ($sync_flags !== 0) {
      drupal_add_js($module_path . '/js/ip_geoloc_leaflet_sync_content.js', array('weight' => 2));
      drupal_add_css($module_path . '/css/ip_geoloc_leaflet_sync_content.css');
    }
    if ($has_full_screen) {
      // Load the 'leaflet-fullscreen' library, containing JS and CSS.
      if (drupal_add_library('ip_geoloc', 'leaflet-fullscreen')) {
        $map['settings']['fullscreenControl'] = array('position' => 'topright');
      }
    }
    if ($has_mini_map) {
      // Load the 'leaflet-minimap' library, containing JS and CSS.
      // See https://github.com/Norkart/Leaflet-MiniMap for more settings.
      if (drupal_add_library('ip_geoloc', 'leaflet-minimap')) {
        $map['settings']['miniMap'] = array(
          'autoToggleDisplay' => TRUE,
          'height' => $this->options['mini_map']['height'],
          'width' => $this->options['mini_map']['width'],
          'position' => 'bottomright',
          'toggleDisplay' => !empty($this->options['mini_map']['toggle']),
          'zoomAnimation' => FALSE,
          'zoomLevelOffset' => (int) $this->options['mini_map']['zoom_delta'],
          // Superimposed rectangle showing extent of main map on the inset.
          'aimingRectOptions' => array(
            'color' => $this->options['mini_map']['scope_color'],
            'weight' => 3,
            'fillOpacity' => 0.1,
          ),
          // The "shadow" rectangle that shows the new map outline.
          'shadowRectOptions' => array(
            'color' => '#888',
            'weight' => 1,
          ),
        );
      }
    }
    $map['settings']['zoomIndicator'] = $zoom_indicator;
    $map['settings']['zoomOnClick'] = $zoom_on_click;
    $map['settings']['resetControl'] = $reset_control;
    $map['settings']['clusterControl'] = $cluster_control;
    $map['settings']['scaleControl'] = $scale_control;
    if ($has_mini_map || $zoom_indicator || $reset_control || $cluster_control || $scale_control) {
      drupal_add_js($module_path . '/js/ip_geoloc_leaflet_controls.js', array('weight' => 1));
      drupal_add_css($module_path . '/css/ip_geoloc_leaflet_controls.css');
    }
    $map['settings']['openBalloonsOnHover'] = $open_balloons_on_hover;
    $map['settings']['gotoContentOnClick'] = $goto_content_on_click;
    if ($open_balloons_on_hover || $goto_content_on_click) {
      drupal_add_js($module_path . '/js/ip_geoloc_leaflet_goto_content_on_click.js', array('scope' => 'footer'));
    }
    $settings = array(
      'mapId' => $map_id,
      'map' => $map,
      'features' => $features,
    );
    $options = array(
      'type' => 'setting',
      // 'footer' only works for type 'inline'.
      'scope' => 'footer',
    );
    drupal_add_js(array('leaflet' => array($settings)), $options);

    libraries_load('leaflet');

    // Little hacky this, but can't see another way to load libraries for
    // Leaflet More Maps, Leaflet MarkerCluster, Leaflet Hash...
    drupal_alter('leaflet_map_prebuild', $settings);

    if ($reset_control || $cluster_control || !empty($has_special_markers)) {
      // Load the CSS that comes with the font icon library which in return
      // tells the browser to fetch either the WOFF, TTF or SVG files that
      // define the font faces.
      drupal_add_library('ip_geoloc', 'ip_geoloc_font_icon_libs');
      drupal_add_css($module_path . '/css/ip_geoloc_leaflet_markers.css');
      drupal_add_js($module_path . '/js/ip_geoloc_leaflet_tagged_marker.js', array('weight' => 1));
    }
    if ($zoom_on_click) {
      drupal_add_js($module_path . '/js/ip_geoloc_leaflet_zoom_on_click.js', array('scope' => 'footer'));
    }

    $output = theme('ip_geoloc_leaflet', array(
      'map_id' => $map_id,
      'height' => trim($this->options['map_height']),
      'view' => $this->view,
    ));

    ip_geoloc_debug(t('-- Leaflet map preparation time: %sec s', array('%sec' => number_format(microtime(TRUE) - $render_start, 2))));
    return $output;
  }

  /**
   * Extract an array of locations.
   *
   * @param bool $enable_balloons
   *   Whether to enable balloons.
   *
   * @return array
   *   array of location objects, each containing lat/long and balloon_text
   */
  protected function extractLocations($enable_balloons = TRUE) {

    $latitudes = $this->options['ip_geoloc_views_plugin_latitude'];
    if (!is_array($latitudes)) {
      $latitudes = array($latitudes);
    }
    $longitude = trim($this->options['ip_geoloc_views_plugin_longitude']);

    $view = &$this->view;
    $view_id = $view->name;
    $display_id = $this->display->id;

    if ($this->display->handler->is_defaulted('style_options')) {
      // $display_id = 'default'.
    }

    $differentiator = FALSE;
    if (!empty($this->options['differentiator']['differentiator_field'])) {
      $differentiator = $this->options['differentiator']['differentiator_field'];
      $is_tax_term = ip_geoloc_is_taxonomy_term($differentiator);
      $diff_color_ass = $this->getColorMappings($this);
      if (empty($diff_color_ass[$display_id])) {
        $diff_color_ass[$display_id] = array($differentiator => array());
      }
    }
    if (!empty($this->options['cluster_aggregation']['aggregation_field'])) {
      $aggregation_field = $this->options['cluster_aggregation']['aggregation_field'];
      if (isset($view->field[$aggregation_field]->definition['type'])) {
        $is_count = (substr($view->field[$aggregation_field]->definition['type'], 0, 4) == 'list');
      }
    }
    $tooltip_field = FALSE;
    if (isset($this->options['tooltips']['marker_tooltip'])) {
      $marker_tooltip = $this->options['tooltips']['marker_tooltip'];
      if (isset($view->field[$marker_tooltip])) {
        $tooltip_field = $view->field[$marker_tooltip];
      }
    }
    $tag_field = FALSE;
    if (isset($this->options['tags']['marker_tag'])) {
      $marker_tag = $this->options['tags']['marker_tag'];
      if (isset($view->field[$marker_tag])) {
        $tag_field = $view->field[$marker_tag];
      }
    }
    $locations = array();
    $error_count = $first_error = 0;
    $loc_field_names = $loc_field_aliases = array();
    foreach ($latitudes as $latitude) {
      if (isset($view->field[$latitude])) {
        // $loc_field_names[] = $view->field[$latitude]->definition
        // ['field_name'].
        // Search API [#2603450]
        $loc_field_names[] = $view->field[$latitude]->field;
        $loc_field_aliases[] = $view->field[$latitude]->field_alias;
        // Example: $loc_field_name == 'field_geo'; $loc_field_alias == 'nid'.
      }
    }

    // Group the rows according to the grouping instructions, if specified.
    $sets = $this->render_grouping(
      $this->view->result,
      $this->options['grouping'],
      TRUE
    );
    if (!empty($this->options['grouping'])) {
      // @todo - Add support for multiple grouping fields?
      $group_field = $this->options['grouping'][0]['field'];
    }

    $separator = '<br/>';
    if (is_a($this, 'ip_geoloc_plugin_style_map')) {
      // Google Map options come as a JSON string.
      if ($map_options = json_decode($this->options['map_options'])) {
        if (isset($map_options->separator)) {
          $separator = filter_xss_admin($map_options->separator);
        }
      }
    }
    elseif (isset($this->options['map_options']['separator'])) {
      $separator = filter_xss_admin($this->options['map_options']['separator']);
    }

    foreach ($sets as $set) {
      // Render as a grouping set.
      if (!empty($set['group'])) {
        // This is required for some Views functions.
        $view->row_index = $i = key($set['rows']);

        $loc_field_name = reset($loc_field_names);
        $loc_field_alias = reset($loc_field_aliases);
        $j = 0;
        $row = reset($set['rows']);
        foreach ($latitudes as $latitude) {

          $base = _ip_geoloc_plugin_style_get_base1($row, $loc_field_name, $loc_field_alias);
          $location = new stdClass();
          $is_location = FALSE;
          if (_ip_geoloc_plugin_style_extract_lat_lng($location, $row, $latitude, $longitude, $base)) {
            $is_location = TRUE;
          }
          // Search API support [#2603450].
          elseif (isset($row->_entity_properties)) {
            $longitude_alias = $view->field[$longitude]->field_alias;
            if (isset($row->_entity_properties[$loc_field_alias])) {
              $location->latitude = $row->_entity_properties[$loc_field_alias];
            }
            if (isset($row->_entity_properties[$longitude_alias])) {
              $location->longitude = $row->_entity_properties[$longitude_alias];
            }
            if (isset($location->latitude) && isset($location->longitude)) {
              $is_location = TRUE;
            }
          }
          if ($is_location) {
            // Remaining row values go into the balloon.
            if ($enable_balloons && !empty($this->rendered_fields[$i])) {
              $location->balloon_text = $this->rendered_fields[$i][$group_field];
              foreach ($set['rows'] as $i => $row) {
                $rendered_fields = $this->rendered_fields[$i];
                unset($rendered_fields[$group_field]);
                $location->balloon_text .= $separator . implode($separator, $rendered_fields);
              }
            }
            if (!empty($diff_color_ass[$display_id][$differentiator])) {
              _ip_geoloc_plugin_style_set_marker_color($location, $row, $differentiator, $is_tax_term, $view->args, $diff_color_ass[$display_id][$differentiator]);
            }
            _ip_geoloc_plugin_style_decorate($location, $row, $tooltip_field, $tag_field);

            if (isset($aggregation_field)) {
              $value = ip_geoloc_get_view_result($this, $aggregation_field, $i);
              $location->aggregation_value = empty($is_count) ? reset($value) : count($value);
            }
            $locations[$j ? "$i.$j" : $i] = $location;
            $j++;
          }

          $loc_field_name = next($loc_field_names);
          $loc_field_alias = next($loc_field_aliases);
        }
        if ($j === 0 && $error_count++ === 0) {
          $first_error = $i;
        }
      }
      // Render as a record set.
      else {
        foreach ($view->result as $i => $row) {
          // This is required for some Views functions.
          $view->row_index = $i;

          $loc_field_name = reset($loc_field_names);
          $loc_field_alias = reset($loc_field_aliases);
          $j = 0;
          foreach ($latitudes as $latitude) {

            $base = _ip_geoloc_plugin_style_get_base1($row, $loc_field_name, $loc_field_alias);
            $location = new stdClass();
            $is_location = FALSE;

            if (_ip_geoloc_plugin_style_extract_lat_lng($location, $row, $latitude, $longitude, $base)) {
              $is_location = TRUE;
            }
            // Search API support [#2603450].
            elseif (isset($row->_entity_properties)) {
              $longitude_alias = $view->field[$longitude]->field_alias;
              if (isset($row->_entity_properties[$loc_field_alias])) {
                $location->latitude = $row->_entity_properties[$loc_field_alias];
              }
              if (isset($row->_entity_properties[$longitude_alias])) {
                $location->longitude = $row->_entity_properties[$longitude_alias];
              }
              if (isset($location->latitude) && isset($location->longitude)) {
                $is_location = TRUE;
              }
            }
            if ($is_location) {
              // Remaining row values go into the balloon.
              if ($enable_balloons && !empty($this->rendered_fields[$i])) {
                $location->balloon_text = implode($separator, $this->rendered_fields[$i]);
              }
              if (!empty($diff_color_ass[$display_id][$differentiator])) {
                _ip_geoloc_plugin_style_set_marker_color($location, $row, $differentiator, $is_tax_term, $view->args, $diff_color_ass[$display_id][$differentiator]);
              }
              _ip_geoloc_plugin_style_decorate($location, $row, $tooltip_field, $tag_field);
              if (isset($aggregation_field)) {
                $value = ip_geoloc_get_view_result($this, $aggregation_field, $i);
                $location->aggregation_value = empty($is_count) ? reset($value) : count($value);
              }
              $locations[$j ? "$i.$j" : $i] = $location;
              $j++;
            }
            $loc_field_name = next($loc_field_names);
            $loc_field_alias = next($loc_field_aliases);
          }
          if ($j === 0 && $error_count++ === 0) {
            $first_error = $i;
            if (isset($row->nid)) {
              $id = 'nid #' . $row->nid;
            }
            if (isset($row->node_title)) {
              $id .= ': ' . $row->node_title;
            }
          }
        }
      }
    }
    global $user;
    if ($error_count > 0 && (($is_debug = ip_geoloc_debug_flag()) || $user->uid == 1)) {
      $row_count = count($view->result);
      $title = empty($this->display->display_options['title']) ? $view->get_human_name() . ' (' . $this->display->display_title . ')' : $this->display->display_options['title'];
      $t_args = array(
        '%view_title' => $title,
        '@total' => $row_count,
        '@error_count' => $error_count,
        '@first' => $first_error + 1,
        '%id' => isset($id) ? " ($id)" : '',
        '%latitudes' => implode(' ' . t('or') . ' ', $latitudes),
      );
      $msg = ($error_count >= $row_count)
        ? t('None of the @total result rows in view %view_title had their %latitudes set. Therefore those rows could not be displayed as locations on the map. Are you using the correct field names?', $t_args)
        : t('Out of a total of @total result rows in view %view_title, @error_count rows did not have their %latitudes set and therefore could not be shown on the map. The first row that could not be located was row #@first%id. You can improve execution efficiency by using the Views UI to add an "is not empty" filter for %latitudes.', $t_args);
      drupal_set_message($msg);

      if ($is_debug) {
        $row_contents = t('First error row:') . '<br/>';
        foreach ((array) $view->result[$first_error] as $field_name => $value) {
          if ($field_name != '_field_data') {
            $output = filter_xss_admin(print_r($value, TRUE));
            $output = drupal_substr($output, 0, 255) . (strlen($output) > 255 ? '...' : '');
            $row_contents .= "<strong>$field_name</strong> = $output<br/>";
          }
        }
        drupal_set_message($row_contents);
      }
    }
    // Allow other modules to implement
    // hook_ip_geoloc_marker_locations_alter(&$locations, &$view)
    drupal_alter('ip_geoloc_marker_locations', $locations, $view);

    return $locations;
  }

  /**
   * Dynamically map colors to taxons.
   */
  protected function getColorMappings() {
    $organisms = mgis_inaturalist_organism_color($this->view->result);
    foreach ($organisms['organisms'] as $organism_id => $value) {
      $association[] = [
        'differentiator_value' => [$organism_id],
        'color' => $value['color'],
        'special char' => '',
        'special char class' => '',
      ];
    }
    $association = [$this->display->id => ['stock_organism_id' => $association]];

    return $association;
  }

}
