<?php

/**
 * @file
 * Contains MgisSearchAccessionTypeFilter.
 */

/**
 * Represents a data alteration that restricts entity indexes to some bundles.
 */
class MgisSearchAccessionTypeFilter extends SearchApiAbstractAlterCallback {

  /**
   * {@inheritdoc}
   */
  public function supportsIndex(SearchApiIndex $index) {
    return in_array('chado_stock', $index->options['datasource']['bundles']);
  }

  /**
   * {@inheritdoc}
   */
  public function configurationForm() {
    $form = [];

    $stock_options = tripal_get_cvterm_default_select_options('stock', 'type_id', 'Stock');
    $form['default'] = [
      '#type' => 'radios',
      '#title' => t('Which items should be indexed?'),
      '#default_value' => isset($this->options['default']) ? $this->options['default'] : 1,
      '#options' => [
        1 => t('All but those from one of the selected accession types'),
        0 => t('Only those from the selected accession types'),
      ],
    ];
    $form['accession_types'] = [
      '#type' => 'select',
      '#title' => t('Accession types'),
      '#default_value' => isset($this->options['accession_types']) ? $this->options['accession_types'] : [],
      '#options' => $stock_options,
      '#size' => min(4, count($stock_options)),
      '#multiple' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function alterItems(array &$items) {
    if (!$this->supportsIndex($this->index) || !isset($this->options['accession_types'])) {
      return;
    }

    $accession_types = $this->options['accession_types'];
    $default = (bool) $this->options['default'];

    foreach ($items as $id => &$item) {

      if (!isset($item->stock)) {
        continue;
      }

      $type_id = $item->stock->type_id->cvterm_id;
      if (isset($accession_types[$type_id]) == $default) {
        unset($items[$id]);
      }
    }
  }

}
