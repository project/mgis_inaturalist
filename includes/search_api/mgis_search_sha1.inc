<?php

/**
 * @file
 * Contains MgisSearchSha1.
 */

/**
 * Processor for making searches case-insensitive.
 */
class MgisSearchSha1 extends SearchApiAbstractProcessor {

  /**
   * {@inheritdoc}
   */
  protected function processFilterValue(&$value) {
    if (is_string($value)) {
      $value = sha1(trim($value));
    }
  }

}
