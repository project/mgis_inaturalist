<?php

/**
 * @file
 * Views hooks.
 */

/**
 * Implements hook_views_data_alter().
 */
function mgis_inaturalist_views_data_alter(&$data) {
  // Area handlers.
  $data['views']['area_view_stock'] = [
    'title' => t('Stock list'),
    'help' => t('Display stock list in header or footer. Stock ID must be an accessible field in this view.'),
    'area' => [
      'handler' => 'views_handler_area_view_stock',
    ],
  ];
  $data['views']['area_in_situ_csv'] = [
    'title' => t('In situ global export'),
    'help' => t('CSV export link for all in situ observations'),
    'area' => [
      'handler' => 'views_handler_area_in_situ_csv',
    ],
  ];
  $data['views']['area_map_legend'] = [
    'title' => t('Map legend'),
    'help' => t('Musa in situ map legend'),
    'area' => [
      'handler' => 'views_handler_area_map_legend',
    ],
  ];
  // Field handlers.
  $data['stockprop']['value']['field']['handler'] = 'views_handler_field_chadoprop';
  $data['stock']['organism_id']['field']['handler'] = 'views_handler_field_organism_id';

  $data['gbif_occurrence']['import_status'] = [
    'title' => t('Import status'),
    'help' => t('GBIF import status.'),
    'field' => [
      'handler' => 'views_handler_gbif_field_import_status',
      'click sortable' => TRUE,
    ],
  ];
}

/**
 * Implements hook_views_plugins().
 */
function mgis_inaturalist_views_plugins() {
  module_load_include('inc', 'ip_geoloc', 'views/ip_geoloc.views');
  $path = drupal_get_path('module', 'mgis_inaturalist');
  $plugins = ip_geoloc_views_plugins();
  $style_plugin = $plugins['style']['ip_geoloc_leaflet'];
  unset($plugins['style']);
  $style_plugin['title'] .= ' [MGIS]';
  $style_plugin['handler'] = 'mgis_inaturalist_plugin_style_leaflet';
  $style_plugin['path'] = $path . '/includes/views/style_plugins';
  $plugins['style']['mgis_inaturalist_leaflet'] = $style_plugin;

  return $plugins;
}
