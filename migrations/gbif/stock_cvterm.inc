<?php

/**
 * @file
 * Migration de liens entre cvterm et stock.
 */

/**
 * Migration de liens entre cvterm et stock.
 */
abstract class MigrateGbifStockCvtermMigration extends MigrateGbifMigration {

  /**
   * Déclaration des paramètres de la migration.
   */
  public function __construct(array $arguments) {
    parent::__construct($arguments);

    $this->destination = new MigrateDestinationChadoFooCvterm('stock');

    $this->map = new MigrateSQLMap($this->machineName, [
      'key' => [
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'description' => t('Source ID'),
      ],
    ],
    MigrateDestinationChadoFooCvterm::getKeySchema('stock')
    );

    $this->addFieldMapping('stock_id', 'key')->sourceMigration('gbif_stock');
    $this->addFieldMapping('pub_id')->defaultValue(1);
  }

}
