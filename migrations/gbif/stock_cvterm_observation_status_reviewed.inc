<?php

/**
 * @file
 * Migration de liens entre cvterm et stock.
 */

/**
 * Lien avec termes « observation reviewed ».
 *
 * Par défaut, toutes ces observations sont catégorisées en reviewed = FALSE.
 */
class MigrateGbifStockCvtermObservationStatusReviewedMigration extends MigrateGbifStockCvtermMigration {

  /**
   * Déclaration des paramètres de la migration.
   */
  public function __construct(array $arguments) {
    parent::__construct($arguments);

    $this->addFieldMapping('cvterm_id')->defaultValue(MGIS_INATURALIST_CVTERM_REVIEWED);
    $this->addFieldMapping('is_not')->defaultValue(TRUE);
  }

}
