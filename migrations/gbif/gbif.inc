<?php

/**
 * @file
 * Migration des données du GBIF sélectionnées.
 */

/**
 * Migration des données du GBIF sélectionnées.
 */
abstract class MigrateGbifMigration extends Migration {

  /**
   * Fields in $row.
   *
   * @var array
   */
  protected $fields = [];

  /**
   * SQL query to execute in source database.
   *
   * @var \SelectQueryInterface
   */
  protected $query;

  /**
   * SQL query to execute in source database (record count).
   *
   * @var \SelectQueryInterface
   */
  protected $countQuery = NULL;

  /**
   * Déclaration des paramètres de la migration.
   */
  public function __construct(array $arguments) {
    parent::__construct($arguments);

    $this->setFields();
    $this->setQuery();
    $this->source = new MigrateSourceSQL($this->query, $this->fields, $this->countQuery, ['map_joinable' => FALSE]);
  }

  /**
   * Sets fields used in $row.
   */
  protected function setFields() {
    $this->fields = [
      'key' => t('ID of GBIF occurence'),
      'status' => 'Status (staged for migration, rejected, etc.)',
      'classification' => t('Classification to use at migration time (organism ID)'),
      'note' => t('Note to be added at migration time'),
      'image_count' => t('Image count'),
      // Added in prepareRow().
      'datasetKey' => t('Dataset key'),
      'publishingOrgKey' => t('Publishing organization  key'),
      'installationKey' => t('Installation key'),
      'publishingCountry' => t('Publishing country'),
      'protocol' => t('Protocol'),
      'lastCrawled' => t('Last crawled'),
      'lastParsed' => t('Last parsed'),
      'crawlId' => t('Crawl ID'),
      'hostingOrganizationKey' => t('Hosting organization key'),
      'extensions' => t('Extensions'),
      'basisOfRecord' => t('Basis of record'),
      'occurrenceStatus' => t('Occurrence status'),
      'taxonKey' => t('Taxon key'),
      'kingdomKey' => t('Kingdom key'),
      'phylumKey' => t('Phylum key'),
      'classKey' => t('Class key'),
      'orderKey' => t('Order Key'),
      'familyKey' => t('Family key'),
      'genusKey' => t('Genus key'),
      'speciesKey' => t('Species key'),
      'acceptedTaxonKey' => t('Accepted taxon key'),
      'scientificName' => t('Scientific name'),
      'acceptedScientificName' => t('Accepted scientific name'),
      'kingdom' => t('Kingdom'),
      'phylum' => t('Phylum'),
      'order' => t('Order'),
      'family' => t('Family'),
      'genus' => t('Genus'),
      'species' => t('Species'),
      'genericName' => t('Generic name'),
      'specificEpithet' => t('Specific epithet'),
      'taxonRank' => t('Taxon rank'),
      'taxonomicStatus' => t('Taxonomic status'),
      'dateIdentified' => t('Date identified'),
      'decimalLongitude' => t('Decimal longitude'),
      'decimalLatitude' => t('Decimal latitude'),
      'coordinateUncertaintyInMeters' => t('Coordinate uncertainty in meters'),
      'stateProvince' => t('State/province'),
      'year' => t('Year'),
      'month' => t('Month'),
      'day' => t('Day'),
      'eventDate' => t('Event date'),
      'issues' => t('Issues'),
      'modified' => t('Modified'),
      'lastInterpreted' => t('Last interpreted'),
      'references' => t('References'),
      'license' => t('License'),
      'identifiers' => t('Identifiers'),
      'media' => t('Media'),
      'facts' => t('Facts'),
      'relations' => t('Relations'),
      'gadm' => t('Gadm'),
      'isInCluster' => t('Is in cluster'),
      'geodeticDatum' => t('Geodetic datum'),
      'class' => t('Class'),
      'countryCode' => t('Country code'),
      'recordedByIDs' => t('Recorded by IDs'),
      'identifiedByIDs' => t('Identified by IDs'),
      'country' => t('Country'),
      'rightsHolder' => t('Rights holder'),
      'identifier' => t('Identifier'),
      'http://unknown.org/nick' => t('Nick name'),
      'verbatimEventDate' => t('Verbatim event date'),
      'datasetName' => t('Dataset name'),
      'gbifID' => t('GBIF ID'),
      'verbatimLocality' => t('Verbatim locality'),
      'collectionCode' => t('Collection code'),
      'occurrenceID' => t('Occurrence ID'),
      'taxonID' => t('Taxon ID'),
      'catalogNumber' => t('Catalog number'),
      'recordedBy' => t('Recorded by'),
      'http://unknown.org/occurrenceDetails' => t('Occurrence details'),
      'institutionCode' => t('Institution Code'),
      'rights' => t('Rights'),
      'eventTime' => t('Event time'),
      'occurrenceRemarks' => t('Occurrence remarks'),
      'identifiedBy' => t('Identified by'),
      'identificationID' => t('Identification ID'),
    ];
  }

  /**
   * Sets SQL query to execute in source database.
   */
  protected function setQuery() {
    $this->query = db_select('gbif_import', 'gi')
      ->fields('gi', ['key', 'status', 'classification', 'note', 'image_count'])
      ->condition('status', MGIS_INATURALIST_GBIF_STAGED_FOR_MIGRATION);
  }

  /**
   * {@inheritdoc}
   */
  public function prepareRow($row) {
    if (parent::prepareRow($row) === FALSE) {
      return FALSE;
    }

    if ($occurrence = gbif_load($row->key)) {
      // Merge values into $row.
      // We could use this method https://stackoverflow.com/a/1953077, which is
      // supposedly faster, but I am reluctant to mess with $row.
      foreach ($occurrence as $key => $value) {
        if (!isset($row->{$key})) {
          $row->{$key} = $value;
        }
      }
    }
    else {
      $this->saveMessage(t('Unable to load GBIF occurrence #@key.', ['@key' => $row->key]));
      return FALSE;
    }

    // Override classification.
    if (!empty($row->classification)) {
      $row->species = $row->classification;
    }

    // Ensure species is set for this occurrence.
    if (!isset($row->species)) {
      $this->saveMessage(t('Species not set for occurrence #@key.', ['@key' => $row->key]));
      return FALSE;
    }
  }

}
