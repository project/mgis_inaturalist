<?php

/**
 * @file
 * Lien entre les observations et le stock.
 */

/**
 * Lien entre les observations et le stock.
 */
class MigrateGbifExperimentStockMigration extends MigrateGbifMigration {

  /**
   * Déclaration des paramètres de la migration.
   */
  public function __construct(array $arguments) {
    parent::__construct($arguments);

    $this->destination = new MigrateDestinationChadoExperimentStock();

    $this->map = new MigrateSQLMap($this->machineName, [
      'key' => [
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'description' => t('Source ID'),
      ],
    ],
    MigrateDestinationChadoExperimentStock::getKeySchema()
    );

    $this->addFieldMapping('nd_experiment_id', 'key')->sourceMigration('gbif_experiment');
    $this->addFieldMapping('stock_id', 'key')->sourceMigration('gbif_stock');
    $this->addFieldMapping('type_id')->defaultValue(MGIS_INATURALIST_CVTERM_CONDUCTED_ON);
  }

}
