<?php

/**
 * @file
 * Création de dbxrefs pour les photos.
 */

/**
 * Création de dbxrefs pour les photos.
 */
class MigrateGbifDbxrefPicture extends MigrateGbifGbifPictureMigration {

  /**
   * Déclaration des paramètres de la migration.
   */
  public function __construct(array $arguments) {
    parent::__construct($arguments);

    $this->destination = new MigrateDestinationChadoDbxref();

    $this->map = new MigrateSQLMap($this->machineName, [
      'key' => [
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'description' => t('Source ID'),
      ],
      'image_delta' => [
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'description' => t('Image delta'),
      ],
    ],
    MigrateDestinationChadoDbxref::getKeySchema()
    );

    $this->addFieldMapping('db_id')->defaultValue(MGIS_INATURALIST_DB_TRAITS);
    $this->addFieldMapping('accession', 'accession');
    $this->addFieldMapping('version')->defaultValue('');
    $this->addFieldMapping('description')->defaultValue('');
  }

  /**
   * {@inheritdoc}
   */
  public function prepareRow($row) {
    if (parent::prepareRow($row) === FALSE) {
      return FALSE;
    }

    $row->accession = 'gbif/' . $row->destination_file;
  }

}
