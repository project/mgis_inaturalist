<?php

/**
 * @file
 * Migration de propriétés.
 */

/**
 * Prendre en charge le département du lieu de l'observation.
 */
class MigrateGbifStockPropCounty extends MigrateGbifMigration {

  /**
   * Déclaration des paramètres de la migration.
   */
  public function __construct(array $arguments) {
    parent::__construct($arguments);

    $this->destination = new MigrateDestinationChadoProp('stock');

    $this->map = new MigrateSQLMap($this->machineName, [
      'key' => [
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'description' => t('Source ID'),
      ],
    ],
    MigrateDestinationChadoProp::getKeySchema('stock')
    );

    $this->addFieldMapping('stock_id', 'key')->sourceMigration('gbif_stock');
    $this->addFieldMapping('type_id')->defaultValue(MGIS_INATURALIST_CVTERM_INATURALIST_COUNTY);
    $this->addFieldMapping('value', 'county');
  }

  /**
   * {@inheritdoc}
   */
  public function prepareRow($row) {
    if (parent::prepareRow($row) === FALSE) {
      return FALSE;
    }

    // Skip if no county.
    if (!isset($row->gadm['level2']['name'])) {
      return FALSE;
    }

    $row->county = $row->gadm['level2']['name'];
  }

}
