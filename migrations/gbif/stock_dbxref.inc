<?php

/**
 * @file
 * Lien entre stock et dbxref précédemment créé.
 */

/**
 * Lien entre stock et dbxref précédemment créé.
 */
class MigrateGbifStockDbxrefMigration extends MigrateGbifMigration {

  /**
   * Déclaration des paramètres de la migration.
   */
  public function __construct(array $arguments) {
    parent::__construct($arguments);

    $this->destination = new MigrateDestinationChadoFooDbxref('stock');

    $this->map = new MigrateSQLMap($this->machineName, [
      'key' => [
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'description' => t('Source ID'),
      ],
    ],
    MigrateDestinationChadoFooDbxref::getKeySchema('stock')
    );

    $this->addFieldMapping('stock_id', 'key')->sourceMigration('gbif_stock');
    $this->addFieldMapping('dbxref_id', 'key')->sourceMigration('gbif_dbxref_stock');
  }

}
