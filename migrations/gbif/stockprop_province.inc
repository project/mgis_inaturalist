<?php

/**
 * @file
 * Migration de propriétés.
 */

/**
 * Prendre en charge la province du lieu de l'observation.
 */
class MigrateGbifStockPropProvince extends MigrateGbifMigration {

  /**
   * Déclaration des paramètres de la migration.
   */
  public function __construct(array $arguments) {
    parent::__construct($arguments);

    $this->destination = new MigrateDestinationChadoProp('stock');

    $this->map = new MigrateSQLMap($this->machineName, [
      'key' => [
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'description' => t('Source ID'),
      ],
    ],
    MigrateDestinationChadoProp::getKeySchema('stock')
    );

    $this->addFieldMapping('stock_id', 'key')->sourceMigration('gbif_stock');
    $this->addFieldMapping('type_id')->defaultValue(MGIS_INATURALIST_CVTERM_INATURALIST_PROVINCE);
    $this->addFieldMapping('value', 'stateProvince');
  }

  /**
   * {@inheritdoc}
   */
  public function prepareRow($row) {
    if (parent::prepareRow($row) === FALSE) {
      return FALSE;
    }

    // Skip if no province.
    if (empty($row->province)) {
      return FALSE;
    }
  }

}
