<?php

/**
 * @file
 * Migration de propriétés.
 */

/**
 * Propriétés « user login ».
 */
class MigrateGbifStockPropUserLogin extends MigrateGbifMigration {

  /**
   * Déclaration des paramètres de la migration.
   */
  public function __construct(array $arguments) {
    parent::__construct($arguments);

    $this->destination = new MigrateDestinationChadoProp('stock');

    $this->map = new MigrateSQLMap($this->machineName, [
      'key' => [
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'description' => t('Source ID'),
      ],
    ],
    MigrateDestinationChadoProp::getKeySchema('stock')
    );

    $this->addFieldMapping('stock_id', 'key')->sourceMigration('gbif_stock');
    $this->addFieldMapping('type_id')->defaultValue(MGIS_INATURALIST_CVTERM_INATURALIST_LOGIN);
    $this->addFieldMapping('value', 'username');
  }

  /**
   * {@inheritdoc}
   */
  public function prepareRow($row) {
    if (parent::prepareRow($row) === FALSE) {
      return FALSE;
    }

    // Skip if no user login.
    if (empty($row->recordedBy)) {
      return FALSE;
    }

    // Hash user login: we only want to allow a user to find back observations
    // imported from GBIF, we do not care about their data.
    $row->username = sha1($row->recordedBy);
  }

}
