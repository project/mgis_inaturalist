<?php

/**
 * @file
 * Migration des observations pour GBIF (geolocation).
 */

/**
 * Migration des géolocalisations pour GBIF.
 */
class MigrateGbifGeolocationMigration extends MigrateGbifMigration {

  /**
   * Déclaration des paramètres de la migration.
   */
  public function __construct(array $arguments) {
    parent::__construct($arguments);

    $this->destination = new MigrateDestinationChadoGeolocation();

    $this->map = new MigrateSQLMap($this->machineName, [
      'key' => [
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'description' => t('Source ID'),
      ],
    ],
    MigrateDestinationChadoGeolocation::getKeySchema()
    );

    $this->addFieldMapping('latitude', 'decimalLatitude');
    $this->addFieldMapping('longitude', 'decimalLongitude');
    $this->addFieldMapping('geodetic_datum', 'geodeticDatum');
  }

}
