<?php

/**
 * @file
 * Migration des photos depuis le GBIF.
 */

/**
 * Migration des photos depuis le GBIF.
 */
abstract class MigrateGbifGbifPictureMigration extends MigrateGbifMigration {

  /**
   * {@inheritdoc}
   */
  public function __construct(array $arguments) {
    $this->setCountQuery();

    parent::__construct($arguments);
  }

  /**
   * {@inheritdoc}
   */
  protected function setFields() {
    parent::setFields();

    $this->fields['image_delta'] = t('Image delta');
  }

  /**
   * {@inheritdoc}
   */
  protected function setQuery() {
    parent::setQuery();

    // Generate a record for each picture (this syntax only works with
    // Postgresql).
    $this->query->addExpression('generate_series(0, image_count - 1)', 'image_delta');
  }

  /**
   * Set SQL count query.
   */
  protected function setCountQuery() {
    $this->countQuery = db_select('gbif_import', 'gi')
      ->condition('status', MGIS_INATURALIST_GBIF_STAGED_FOR_MIGRATION);
    $this->countQuery->addExpression('SUM(image_count)', 'sum');
  }

  /**
   * {@inheritdoc}
   */
  public function prepareRow($row) {
    if (parent::prepareRow($row) === FALSE) {
      return FALSE;
    }

    // Skip if this occurrence has no pictures.
    if (empty($row->media)) {
      return FALSE;
    }

    if (!empty($row->media[$row->image_delta]['rightsHolder'])) {
      $row->attribution = '(c) ' . $row->media[$row->image_delta]['rightsHolder'];
    }
    if (!empty($row->media[$row->image_delta]['license'])) {
      $row->attribution .= ' (' . $row->media[$row->image_delta]['license'] . ')';
    }

    $row->picture_url = $row->media[$row->image_delta]['identifier'];
    $row->destination_file = $row->key . '-' . $row->image_delta . '.jpg';
  }

}
