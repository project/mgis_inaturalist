<?php

/**
 * @file
 * Lien entre stock et dbxref d'une image précédemment créé.
 */

/**
 * Lien entre stock et dbxref d'une image précédemment créé.
 */
class MigrateGbifStockDbxrefPictureMigration extends MigrateGbifGbifPictureMigration {

  /**
   * Déclaration des paramètres de la migration.
   */
  public function __construct(array $arguments) {
    parent::__construct($arguments);

    $this->destination = new MigrateDestinationChadoFooDbxref('stock');

    $this->map = new MigrateSQLMap($this->machineName, [
      'key' => [
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'description' => t('Source ID'),
      ],
      'image_delta' => [
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'description' => t('Image delta'),
      ],
    ],
    MigrateDestinationChadoFooDbxref::getKeySchema('stock')
    );

    $this->addFieldMapping('stock_id', 'key')->sourceMigration('gbif_stock');
    $this->addFieldMapping('dbxref_id', 'dbxref_id');
  }

  /**
   * {@inheritdoc}
   */
  public function prepareRow($row) {
    if (parent::prepareRow($row) === FALSE) {
      return FALSE;
    }

    // Method sourceMigration does not seem to work with multiple source IDs in
    // D7, too bad.
    $row->dbxref_id = db_select('migrate_map_gbif_dbxref_picture', 'mm')
      ->fields('mm', ['destid1'])
      ->condition('sourceid1', $row->key)
      ->condition('sourceid2', $row->image_delta)
      ->execute()
      ->fetchField();
  }

}
