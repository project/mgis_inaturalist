<?php

/**
 * @file
 * Migration des photos du GBIF.
 */

/**
 * Migration des photos du GBIF.
 */
class MigrateGbifPictureMigration extends MigrateGbifGbifPictureMigration {

  /**
   * Déclaration des paramètres de la migration.
   */
  public function __construct(array $arguments) {
    parent::__construct($arguments);

    $this->destination = new MigrateDestinationFile('file', 'MigrateFileUri');

    $this->map = new MigrateSQLMap($this->machineName, [
      'key' => [
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'description' => t('Source ID'),
      ],
      'image_delta' => [
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'description' => t('Image delta'),
      ],
    ],
    MigrateDestinationFile::getKeySchema()
    );

    $this->addFieldMapping('urlencode')->defaultValue(0);
    $this->addFieldMapping('destination_dir')->defaultValue('public://pictures/gbif');
    $this->addFieldMapping('destination_file', 'destination_file');
    $this->addFieldMapping('value', 'picture_url');
  }

}
