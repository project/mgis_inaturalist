<?php

/**
 * @file
 * Migration de liens entre cvterm et stock.
 */

/**
 * Lien avec termes « observation hidden ».
 *
 * Par défaut, toutes les observations sont catégorisées en hidden = FALSE.
 */
class MigrateGbifStockCvtermObservationStatusHiddenMigration extends MigrateGbifStockCvtermMigration {

  /**
   * Déclaration des paramètres de la migration.
   */
  public function __construct(array $arguments) {
    parent::__construct($arguments);

    $this->addFieldMapping('cvterm_id')->defaultValue(MGIS_INATURALIST_CVTERM_HIDDEN);
    $this->addFieldMapping('is_not')->defaultValue(TRUE);
  }

}
