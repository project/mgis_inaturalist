<?php

/**
 * @file
 * Migration de propriétés.
 */

/**
 * Date de l'observation au format ISO 8601.
 */
class MigrateGbifExperimentPropObservedOn extends MigrateGbifMigration {

  /**
   * Déclaration des paramètres de la migration.
   */
  public function __construct(array $arguments) {
    parent::__construct($arguments);

    $this->destination = new MigrateDestinationChadoProp('nd_experiment');

    $this->map = new MigrateSQLMap($this->machineName, [
      'key' => [
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'description' => t('Source ID'),
      ],
    ],
    MigrateDestinationChadoProp::getKeySchema('nd_experiment')
    );

    $this->addFieldMapping('nd_experiment_id', 'key')->sourceMigration('gbif_experiment');
    $this->addFieldMapping('type_id')->defaultValue(MGIS_INATURALIST_CVTERM_OBSERVED_ON);
    $this->addFieldMapping('value', 'eventDate');
  }

  /**
   * {@inheritdoc}
   */
  public function prepareRow($row) {
    if (parent::prepareRow($row) === FALSE) {
      return FALSE;
    }

    if (empty($row->eventDate)) {
      return FALSE;
    }
  }

}
