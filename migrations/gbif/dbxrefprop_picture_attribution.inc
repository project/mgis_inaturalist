<?php

/**
 * @file
 * Migration de propriétés.
 */

/**
 * Propriétés « attribution ».
 */
class MigrateGbifDbxrefPropAttribution extends MigrateGbifGbifPictureMigration {

  /**
   * Déclaration des paramètres de la migration.
   */
  public function __construct(array $arguments) {
    parent::__construct($arguments);

    $this->destination = new MigrateDestinationChadoProp('dbxref');

    $this->map = new MigrateSQLMap($this->machineName, [
      'key' => [
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'description' => t('Source ID'),
      ],
      'image_delta' => [
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'description' => t('Image delta'),
      ],
    ],
    MigrateDestinationChadoProp::getKeySchema('dbxref')
    );

    $this->addFieldMapping('dbxref_id', 'dbxref_id');
    $this->addFieldMapping('type_id')->defaultValue(MGIS_INATURALIST_CVTERM_PHOTO_ATTRIBUTION);
    $this->addFieldMapping('value', 'attribution');
  }

  /**
   * {@inheritdoc}
   */
  public function prepareRow($row) {
    if (parent::prepareRow($row) === FALSE) {
      return FALSE;
    }

    if (empty($row->attribution)) {
      return FALSE;
    }

    // Method sourceMigration does not seem to work with multiple source IDs in
    // D7, too bad.
    $row->dbxref_id = db_select('migrate_map_gbif_dbxref_picture', 'mm')
      ->fields('mm', ['destid1'])
      ->condition('sourceid1', $row->key)
      ->condition('sourceid2', $row->image_delta)
      ->execute()
      ->fetchField();
  }

}
