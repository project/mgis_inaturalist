<?php

/**
 * @file
 * Création d'un dbxref pour chaque observation issue du GBIF.
 */

/**
 * Création d'un dbxref pour chaque observation issue du GBIF.
 */
class MigrateGbifDbxrefStockMigration extends MigrateGbifMigration {

  /**
   * Déclaration des paramètres de la migration.
   */
  public function __construct(array $arguments) {
    parent::__construct($arguments);

    $this->destination = new MigrateDestinationChadoDbxref();

    $this->map = new MigrateSQLMap($this->machineName, [
      'key' => [
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'description' => t('Source ID'),
      ],
    ],
    MigrateDestinationChadoDbxref::getKeySchema()
    );

    $this->addFieldMapping('db_id')->defaultValue(MGIS_INATURALIST_DB_GBIF_OBSERVATION);
    $this->addFieldMapping('accession', 'key');
    $this->addFieldMapping('version')->defaultValue('');
    $this->addFieldMapping('description')->defaultValue('');
  }

}
