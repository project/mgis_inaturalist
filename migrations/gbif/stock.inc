<?php

/**
 * @file
 * Migration pour GBIF (stock).
 */

/**
 * Migration des observations (stock) pour GBIF.
 */
class MigrateGbifStockMigration extends MigrateGbifMigration {

  /**
   * Déclaration des paramètres de la migration.
   */
  public function __construct(array $arguments) {
    parent::__construct($arguments);

    $this->destination = new MigrateDestinationChadoStock();

    $this->map = new MigrateSQLMap($this->machineName, [
      'key' => [
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'description' => t('Source ID'),
      ],
    ],
    MigrateDestinationChadoStock::getKeySchema()
    );

    $this->addFieldMapping('type_id')->defaultValue(MGIS_INATURALIST_CVTERM_GBIF_IMPORT);
    $this->addFieldMapping('name', 'species');
    $this->addFieldMapping('uniquename', 'uniquename');
    $this->addFieldMapping('organism_id', 'species')->sourceMigration('gbif_taxonomy');
  }

  /**
   * {@inheritdoc}
   */
  public function prepareRow($row) {
    if (parent::prepareRow($row) === FALSE) {
      return FALSE;
    }

    // Generate unique name for this observation.
    $row->uniquename = 'GBIFIMPORT' . $row->key;
  }

}
