<?php

/**
 * @file
 * Migration de propriétés.
 */

/**
 * Code pays.
 *
 * Prendre en charge le code ISO 3166-1 alpha-3 de l'État où l'observation a
 * été effectuée.
 */
class MigrateGbifStockPropCountry extends MigrateGbifMigration {

  /**
   * Déclaration des paramètres de la migration.
   */
  public function __construct(array $arguments) {
    parent::__construct($arguments);

    $this->destination = new MigrateDestinationChadoProp('stock');

    $this->map = new MigrateSQLMap($this->machineName, [
      'key' => [
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'description' => t('Source ID'),
      ],
    ],
    MigrateDestinationChadoProp::getKeySchema('stock')
    );

    $this->addFieldMapping('stock_id', 'key')->sourceMigration('gbif_stock');
    $this->addFieldMapping('type_id')->defaultValue(MGIS_INATURALIST_CVTERM_INATURALIST_COUNTRY);
    $this->addFieldMapping('value', 'country_code');
  }

  /**
   * {@inheritdoc}
   */
  public function prepareRow($row) {
    if (parent::prepareRow($row) === FALSE) {
      return FALSE;
    }

    // Convert 2-letter country code to 3-letter form.
    $row->country_code = mgis_country_a2_to_a3($row->countryCode);
  }

}
