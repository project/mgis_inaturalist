<?php

/**
 * @file
 * Migration de propriétés.
 */

/**
 * Propriété « uncertainty » (incertitude relative à la localisation).
 */
class MigrateGbifStockPropUncertainty extends MigrateGbifMigration {

  /**
   * Déclaration des paramètres de la migration.
   */
  public function __construct(array $arguments) {
    parent::__construct($arguments);

    $this->destination = new MigrateDestinationChadoProp('stock');

    $this->map = new MigrateSQLMap($this->machineName, [
      'key' => [
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'description' => t('Source ID'),
      ],
    ],
    MigrateDestinationChadoProp::getKeySchema('stock')
    );

    $this->addFieldMapping('stock_id', 'key')->sourceMigration('gbif_stock');
    $this->addFieldMapping('type_id')->defaultValue(MGIS_INATURALIST_CVTERM_UNCERTAINTY);
    $this->addFieldMapping('value', 'uncertainty_level');
  }

  /**
   * {@inheritdoc}
   */
  public function prepareRow($row) {
    if (parent::prepareRow($row) === FALSE) {
      return FALSE;
    }

    // Skip if no uncertainty.
    if (empty($row->coordinateUncertaintyInMeters)) {
      return FALSE;
    }

    // @see https://wiki.openstreetmap.org/wiki/Zoom_levels#Distance_per_pixel_math
    $zoom = floor(log(40075016.686 / $row->coordinateUncertaintyInMeters) / log(2));

    if ($zoom < 3) {
      $level = 'world';
    }
    elseif ($zoom < 6) {
      $level = 'country';
    }
    elseif ($zoom < 11) {
      $level = 'region';
    }
    elseif ($zoom < 12) {
      $level = 'city';
    }
    elseif ($zoom < 16) {
      $level = 'village';
    }
    else {
      $level = 'street';
    }
    $row->uncertainty_level = $level . ' ' . $zoom;
  }

}
