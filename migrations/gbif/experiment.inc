<?php

/**
 * @file
 * Migration pour GBIF (experiment).
 */

/**
 * Migration des observations (experiment) pour GBIF.
 */
class MigrateGbifExperimentMigration extends MigrateGbifMigration {

  /**
   * Déclaration des paramètres de la migration.
   */
  public function __construct(array $arguments) {
    parent::__construct($arguments);

    $this->destination = new MigrateDestinationChadoExperiment();

    $this->map = new MigrateSQLMap($this->machineName, [
      'key' => [
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'description' => t('Source ID'),
      ],
    ],
    MigrateDestinationChadoExperiment::getKeySchema()
    );

    $this->addFieldMapping('nd_geolocation_id', 'key')->sourceMigration('gbif_geolocation');
    $this->addFieldMapping('type_id')->defaultValue(MGIS_INATURALIST_CVTERM_OBSERVATION);
  }

}
