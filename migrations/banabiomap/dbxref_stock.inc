<?php

/**
 * @file
 * Création d'un dbxref pour chaque observation issue de BaNaBioMap.
 */

/**
 * Création d'un dbxref pour chaque observation issue de BaNaBioMap.
 */
class MigrateBanabiomapDbxrefStockMigration extends MigrateBanabiomapMigration {

  /**
   * Déclaration des paramètres de la migration.
   */
  public function __construct(array $arguments) {
    parent::__construct($arguments);

    $this->path = dirname($this->path) . '/dbxref.csv';
    $this->source = new BanabiomapMigrateSourceCSV($this->path, $this->columns, $this->options);

    $this->destination = new MigrateDestinationChadoDbxref();

    $this->map = new MigrateSQLMap($this->machineName, [
      'page_url' => [
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'description' => t('Page URL, used as ID'),
      ],
    ],
    MigrateDestinationChadoDbxref::getKeySchema()
    );

    $this->addFieldMapping('db_id', 'db_id');
    $this->addFieldMapping('accession', 'accession');
    $this->addFieldMapping('version')->defaultValue('');
    $this->addFieldMapping('description')->defaultValue('');
  }

  /**
   * {@inheritdoc}
   */
  public function prepareRow($row) {
    if (parent::prepareRow($row) === FALSE) {
      return FALSE;
    }

    // Skip empty URLs.
    if (empty($row->page_url)) {
      return FALSE;
    }

    switch ($row->source_type) {
      case 'Flickr':
        $regex = '#photos/(.*)$#';
        $db_id = MGIS_INATURALIST_DB_FLICKR_OBSERVATION;
        break;

      case 'GBIF':
        $regex = '#occurrence/(.*)$#';
        $db_id = MGIS_INATURALIST_DB_GBIF_OBSERVATION;
        break;

      default:
        // Extract whole URL.
        $regex = '#(.*)#';
        $db_id = MGIS_INATURALIST_DB_BANABIOMAP_OBSERVATION;
        break;
    }

    // Deduce accession ID from URL.
    $matches = [];
    if (preg_match($regex, $row->page_url, $matches)) {
      $row->accession = $matches[1];
    }
    else {
      return FALSE;
    }

    $row->db_id = $db_id;
  }

}
