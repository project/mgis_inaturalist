<?php

/**
 * @file
 * Migration pour BaNaBioMap (experiment).
 */

/**
 * Migration des observations (experiment) pour BaNaBioMap.
 */
class MigrateBanabiomapExperimentMigration extends MigrateBanabiomapMigration {

  /**
   * Déclaration des paramètres de la migration.
   */
  public function __construct(array $arguments) {
    parent::__construct($arguments);

    $this->destination = new MigrateDestinationChadoExperiment();

    $this->map = new MigrateSQLMap($this->machineName, [
      'id' => [
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'description' => t('Source ID'),
      ],
    ],
    MigrateDestinationChadoExperiment::getKeySchema()
    );

    $this->addFieldMapping('nd_geolocation_id', 'id')->sourceMigration('banabiomap_geolocation');
    $this->addFieldMapping('type_id')->defaultValue(MGIS_INATURALIST_CVTERM_OBSERVATION);
  }

}
