<?php

/**
 * @file
 * Migrations depuis Flickr, GBIF et autres sources (recensées dans un CSV).
 */

/**
 * Migration d'observations depuis Flickr, GBIF et autres sources.
 */
abstract class MigrateBanabiomapMigration extends Migration {

  /**
   * Source file path.
   *
   * @var string
   */
  protected $path;

  /**
   * Source file columns.
   *
   * @var array
   */
  protected $columns;

  /**
   * Source options.
   *
   * @var array
   */
  protected $options;

  /**
   * Déclaration des paramètres de la migration.
   */
  public function __construct(array $arguments) {
    parent::__construct($arguments);

    $this->path = 'public://banabiomap/banabiomap.csv';
    $this->columns = [
      0 => ['id', t('Observation ID')],
      1 => ['genus', t('Genus')],
      2 => ['section', t('Section')],
      3 => ['species', t('Species / group')],
      4 => ['subtaxon', t('Subgroup, subspecies or variety')],
      5 => ['subtaxon_type', t('Subtaxon type (subgroup, subspecies or variety)')],
      6 => ['reliability', t('Reliability')],
      7 => ['name', t('Name')],
      8 => ['country', t('Country')],
      9 => ['province', t('Province / district')],
      10 => ['city', t('Nearest city, town or village')],
      11 => ['location', t('Location')],
      12 => ['latitude', t('Latitude')],
      13 => ['longitude', t('Longitude')],
      14 => ['altitude', t('Altitude')],
      15 => ['uncertainty', t('Uncertainty')],
      16 => ['has_picture', t('Whether a picture is associated to the observation')],
      17 => ['source_type', t('Source type (Flickr, GBIF, etc.)')],
      18 => ['source_name', t('Source name')],
      19 => ['photo_id', t('Flicker photo ID')],
      20 => ['picture_license', t('Picture license')],
      21 => ['picture_url', t('Picture URL')],
      22 => ['observation_license', t('Observation license')],
      23 => ['page_url', t('Page URL')],
      24 => ['username', t('Username')],
      25 => ['year_observed', t('Year observed')],
      26 => ['added', t('Date the observation was added')],
      27 => ['by', t('Person who added this observation')],
      28 => ['remark', t('Remark')],
      29 => ['usability', t('Usability')],
      30 => ['mgis', t('Already referenced in MGIS')],
    ];
    $this->options = [
      'header_rows' => 1,
      'delimiter' => ',',
    ];
    $this->source = new BanabiomapMigrateSourceCSV($this->path, $this->columns, $this->options);
  }

  /**
   * {@inheritdoc}
   */
  public function prepareRow($row) {
    if (parent::prepareRow($row) === FALSE) {
      return FALSE;
    }

    $row->has_picture = $row->has_picture == 'oui';

    $row->latitude = $this->toFloat($row->latitude);
    $row->longitude = $this->toFloat($row->longitude);
    $row->altitude = $this->toFloat($row->altitude);
  }

  /**
   * Deal with comma as decimal separator.
   */
  protected function toFloat($value) {
    return (float) str_replace(',', '.', $value);
  }

}
