<?php

/**
 * @file
 * Migration de propriétés.
 */

/**
 * Propriété « uncertainty » (incertitude relative à la localisation).
 */
class MigrateBanabiomapStockPropUncertainty extends MigrateBanabiomapMigration {

  /**
   * Déclaration des paramètres de la migration.
   */
  public function __construct(array $arguments) {
    parent::__construct($arguments);

    $this->destination = new MigrateDestinationChadoProp('stock');

    $this->map = new MigrateSQLMap($this->machineName, [
      'id' => [
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'description' => t('Source ID'),
      ],
    ],
    MigrateDestinationChadoProp::getKeySchema('stock')
    );

    $this->addFieldMapping('stock_id', 'id')->sourceMigration('banabiomap_stock');
    $this->addFieldMapping('type_id')->defaultValue(MGIS_INATURALIST_CVTERM_UNCERTAINTY);
    $this->addFieldMapping('value', 'uncertainty_level');
  }

  /**
   * {@inheritdoc}
   */
  public function prepareRow($row) {
    if (parent::prepareRow($row) === FALSE) {
      return FALSE;
    }

    // Skip if no uncertainty.
    if (empty($row->uncertainty)) {
      return FALSE;
    }

    $row->uncertainty = (int) $row->uncertainty;
    if ($row->uncertainty < 3) {
      $level = 'world';
    }
    elseif ($row->uncertainty < 6) {
      $level = 'country';
    }
    elseif ($row->uncertainty < 11) {
      $level = 'region';
    }
    elseif ($row->uncertainty < 12) {
      $level = 'city';
    }
    elseif ($row->uncertainty < 16) {
      $level = 'village';
    }
    else {
      $level = 'street';
    }
    $row->uncertainty_level = $level . ' ' . $row->uncertainty;

  }

}
