<?php

/**
 * @file
 * Migration pour BaNaBioMap (geolocation).
 */

/**
 * Migration des géolocalisations pour BaNaBioMap.
 */
class MigrateBanabiomapGeolocationMigration extends MigrateBanabiomapMigration {

  /**
   * Déclaration des paramètres de la migration.
   */
  public function __construct(array $arguments) {
    parent::__construct($arguments);

    $this->destination = new MigrateDestinationChadoGeolocation();

    $this->map = new MigrateSQLMap($this->machineName, [
      'id' => [
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'description' => t('Source ID'),
      ],
    ],
    MigrateDestinationChadoGeolocation::getKeySchema()
    );

    $this->addFieldMapping('latitude', 'latitude');
    $this->addFieldMapping('longitude', 'longitude');
    $this->addFieldMapping('altitude', 'altitude');
    $this->addFieldMapping('geodetic_datum')->defaultValue(MGIS_INATURALIST_GEODETIC_DATUM);
    $this->addFieldMapping('description', 'location');
  }

}
