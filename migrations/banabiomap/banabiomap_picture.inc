<?php

/**
 * @file
 * Migration des photos pour BaNaBioMap.
 */

/**
 * Migration des photos pour BaNaBioMap.
 */
abstract class MigrateBanabiomapBanabiomapPictureMigration extends MigrateBanabiomapMigration {

  /**
   * {@inheritdoc}
   */
  public function prepareRow($row) {
    if (parent::prepareRow($row) === FALSE) {
      return FALSE;
    }

    if (empty($row->picture_url) || empty($row->picture_license) || $row->picture_license == 'all rights reserved') {
      return FALSE;
    }

    $row->destination_file = $row->id . '.jpg';
  }

}
