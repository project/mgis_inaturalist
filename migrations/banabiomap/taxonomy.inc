<?php

/**
 * @file
 * Migration de la taxonomie pour BaNaBioMap.
 *
 * Aucun élément n'est créé dans la base : sert surtout à établir la
 * correspondance par la suite à l'importation du stock.
 *
 * @see MigrateBanabiomapStockMigration
 */

/**
 * Mise en correspondance de la taxonomie.
 */
class MigrateBanabiomapTaxonomyMigration extends Migration {

  /**
   * Déclaration des paramètres de la migration.
   */
  public function __construct(array $arguments) {
    parent::__construct($arguments);

    $csv_path = drupal_get_path('module', 'mgis_inaturalist') . '/data/taxonomy.csv';
    $columns = [
      0 => ['source_name', 'Source name from BaNaBioMap'],
      1 => ['genus', 'Genus name used for the MGIS project'],
      2 => ['species', 'Species name used for the MGIS project'],
    ];
    $options = ['header_rows' => 1];
    $this->source = new MigrateSourceCSV($csv_path, $columns, $options);

    $this->destination = new MigrateDestinationChadoOrganism(TRUE, TRUE);
    // Important: as we choose to allow the creation of new organisms (hybrid
    // mode), prevent destruction on rollback (both pre-existing and imported
    // records).
    // @see MigrateDestination::bulkRollback()
    $this->defaultRollbackAction = MigrateMap::ROLLBACK_PRESERVE;

    $this->map = new MigrateSQLMap($this->machineName, [
      'source_name' => [
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'description' => t('Source name from BaNaBioMap, used as ID'),
      ],
    ],
    MigrateDestinationChadoOrganism::getKeySchema()
    );

    $this->addFieldMapping('genus', 'genus');
    $this->addFieldMapping('species', 'species');
  }

}
