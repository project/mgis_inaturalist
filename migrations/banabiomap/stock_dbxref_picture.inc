<?php

/**
 * @file
 * Lien entre stock et dbxref d'une image précédemment créé.
 */

/**
 * Lien entre stock et dbxref d'une image précédemment créé.
 */
class MigrateBanabiomapStockDbxrefPictureMigration extends MigrateBanabiomapBanabiomapPictureMigration {

  /**
   * Déclaration des paramètres de la migration.
   */
  public function __construct(array $arguments) {
    parent::__construct($arguments);

    $this->destination = new MigrateDestinationChadoFooDbxref('stock');

    $this->map = new MigrateSQLMap($this->machineName, [
      'id' => [
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'description' => t('Source ID'),
      ],
    ],
    MigrateDestinationChadoFooDbxref::getKeySchema('stock')
    );

    $this->addFieldMapping('stock_id', 'id')->sourceMigration('banabiomap_stock');
    $this->addFieldMapping('dbxref_id', 'id')->sourceMigration('banabiomap_dbxref_picture');
  }

}
