<?php

/**
 * @file
 * Migration de propriétés.
 */

/**
 * Prendre en charge la province du lieu de l'observation.
 */
class MigrateBanabiomapStockPropProvince extends MigrateBanabiomapMigration {

  /**
   * Déclaration des paramètres de la migration.
   */
  public function __construct(array $arguments) {
    parent::__construct($arguments);

    $this->destination = new MigrateDestinationChadoProp('stock');

    $this->map = new MigrateSQLMap($this->machineName, [
      'id' => [
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'description' => t('Source ID'),
      ],
    ],
    MigrateDestinationChadoProp::getKeySchema('stock')
    );

    $this->addFieldMapping('stock_id', 'id')->sourceMigration('banabiomap_stock');
    $this->addFieldMapping('type_id')->defaultValue(MGIS_INATURALIST_CVTERM_INATURALIST_PROVINCE);
    $this->addFieldMapping('value', 'province');
  }

  /**
   * {@inheritdoc}
   */
  public function prepareRow($row) {
    if (parent::prepareRow($row) === FALSE) {
      return FALSE;
    }

    // Skip if no province.
    if (empty($row->province)) {
      return FALSE;
    }
  }

}
