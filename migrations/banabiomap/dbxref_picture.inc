<?php

/**
 * @file
 * Création de dbxrefs pour les photos.
 */

/**
 * Création de dbxrefs pour les photos.
 */
class MigrateBanabiomapDbxrefPicture extends MigrateBanabiomapBanabiomapPictureMigration {

  /**
   * Déclaration des paramètres de la migration.
   */
  public function __construct(array $arguments) {
    parent::__construct($arguments);

    $this->destination = new MigrateDestinationChadoDbxref();

    $this->map = new MigrateSQLMap($this->machineName, [
      'id' => [
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'description' => t('Source ID'),
      ],
    ],
    MigrateDestinationChadoDbxref::getKeySchema()
    );

    $this->addFieldMapping('db_id')->defaultValue(MGIS_INATURALIST_DB_TRAITS);
    $this->addFieldMapping('accession', 'id')
      ->sourceMigration('banabiomap_pictures')
      ->callbacks([$this, 'getlocalPath']);
    $this->addFieldMapping('version')->defaultValue('');
    $this->addFieldMapping('description')->defaultValue('');
  }

  /**
   * Get path to be used in dbxref accession column.
   */
  protected function getlocalPath($fid) {
    $file = file_load($fid);
    return 'banabiomap/' . $file->filename;
  }

}
