<?php

/**
 * @file
 * Migration pour BaNaBioMap (stock).
 */

/**
 * Migration des observations (stock) pour BaNaBioMap.
 */
class MigrateBanabiomapStockMigration extends MigrateBanabiomapMigration {
  
  /**
   * Source lookup.
   */
  public static $SOURCE_PREFIX_LOOKUP = [
    ''                   => 'BANABIOMAP',
    'Bibliography'       => 'BIBLIO',
    'Bioversity'         => 'BIOVERSITY',
    'Flickr'             => 'FLICKR',
    'GBIF'               => 'GBIF',
    'GBIF (import)'      => 'GBIFIMPORT',
    'Google Street View' => 'GSV',
    'Other'              => 'BANABIOMAP',
    'Project'            => 'PROJECT',
  ];

  /**
   * Déclaration des paramètres de la migration.
   */
  public function __construct(array $arguments) {
    parent::__construct($arguments);

    $this->destination = new MigrateDestinationChadoStock();

    $this->map = new MigrateSQLMap($this->machineName, [
      'id' => [
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'description' => t('Source ID'),
      ],
    ],
    MigrateDestinationChadoStock::getKeySchema()
    );

    $this->addFieldMapping('type_id')->defaultValue(MGIS_INATURALIST_CVTERM_BANABIOMAP);
    $this->addFieldMapping('name', 'name');
    $this->addFieldMapping('uniquename', 'uniquename');
    $this->addFieldMapping('organism_id', 'organism_name')->sourceMigration('banabiomap_taxonomy');
  }

  /**
   * {@inheritdoc}
   */
  public function prepareRow($row) {
    if (parent::prepareRow($row) === FALSE) {
      return FALSE;
    }

    // Generate unique name for this observation.
    if (array_key_exists($row->source_type, MigrateBanabiomapStockMigration::$SOURCE_PREFIX_LOOKUP)) {
      $prefix = MigrateBanabiomapStockMigration::$SOURCE_PREFIX_LOOKUP[$row->source_type];
    }
    else {
      $prefix = MigrateBanabiomapStockMigration::$SOURCE_PREFIX_LOOKUP[''];
    }
    $row->uniquename = $prefix . $row->id;

    // Organism name. Take ill-determined observations into account, e.g. "Musa"
    // will be internally mapped as "Musa Musa" which stands for "Musa sp.".
    $row->organism_name = $row->genus;
    if (!empty($row->species)) {
      $row->organism_name .= ' ' . $row->species;
      if (!empty($row->subtaxon) && !empty($row->subtaxon_type)) {
        if ($row->subtaxon_type == 'Group') {
          $row->subtaxon_type = 'subgr.';
        }
        $row->organism_name .= ' ' . $row->subtaxon_type . ' ' . $row->subtaxon;
      }
    }
  }

}
