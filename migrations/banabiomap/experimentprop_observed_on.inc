<?php

/**
 * @file
 * Migration de propriétés.
 */

/**
 * Date de l'observation au format ISO 8601.
 */
class MigrateBanabiomapExperimentPropObservedOn extends MigrateBanabiomapMigration {

  /**
   * Déclaration des paramètres de la migration.
   */
  public function __construct(array $arguments) {
    parent::__construct($arguments);

    $this->destination = new MigrateDestinationChadoProp('nd_experiment');

    $this->map = new MigrateSQLMap($this->machineName, [
      'id' => [
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'description' => t('Source ID'),
      ],
    ],
    MigrateDestinationChadoProp::getKeySchema('nd_experiment')
    );

    $this->addFieldMapping('nd_experiment_id', 'id')->sourceMigration('banabiomap_experiment');
    $this->addFieldMapping('type_id')->defaultValue(MGIS_INATURALIST_CVTERM_OBSERVED_ON);
    $this->addFieldMapping('value', 'observed_on');
  }

  /**
   * {@inheritdoc}
   */
  public function prepareRow($row) {
    if (parent::prepareRow($row) === FALSE) {
      return FALSE;
    }

    if (empty($row->year_observed)) {
      return FALSE;
    }

    // Artificially set observation date on January 1.
    $row->observed_on = $row->year_observed . '-01-01';
  }

}
