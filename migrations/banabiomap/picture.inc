<?php

/**
 * @file
 * Migration des photos pour BaNaBioMap.
 */

/**
 * Migration des photos pour BaNaBioMap.
 */
class MigrateBanabiomapPictureMigration extends MigrateBanabiomapBanabiomapPictureMigration {

  /**
   * Déclaration des paramètres de la migration.
   */
  public function __construct(array $arguments) {
    parent::__construct($arguments);

    $this->destination = new MigrateDestinationFile('file', 'MigrateFileUri');

    $this->map = new MigrateSQLMap($this->machineName, [
      'id' => [
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'description' => t('Source ID'),
      ],
    ],
    MigrateDestinationFile::getKeySchema()
    );

    $this->addFieldMapping('destination_dir')->defaultValue('public://pictures/banabiomap');
    $this->addFieldMapping('destination_file', 'destination_file');
    $this->addFieldMapping('value', 'picture_url');
  }

}
