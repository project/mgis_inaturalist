<?php

/**
 * @file
 * Lien entre stock et dbxref précédemment créé.
 */

/**
 * Lien entre stock et dbxref précédemment créé.
 */
class MigrateBanabiomapStockDbxrefMigration extends MigrateBanabiomapMigration {

  /**
   * Déclaration des paramètres de la migration.
   */
  public function __construct(array $arguments) {
    parent::__construct($arguments);

    $this->destination = new MigrateDestinationChadoFooDbxref('stock');

    $this->map = new MigrateSQLMap($this->machineName, [
      'id' => [
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'description' => t('Source ID'),
      ],
    ],
    MigrateDestinationChadoFooDbxref::getKeySchema('stock')
    );

    $this->addFieldMapping('stock_id', 'id')->sourceMigration('banabiomap_stock');
    $this->addFieldMapping('dbxref_id', 'page_url')->sourceMigration('banabiomap_dbxref_stock');
  }

  /**
   * {@inheritdoc}
   */
  public function prepareRow($row) {
    if (parent::prepareRow($row) === FALSE) {
      return FALSE;
    }

    if (empty($row->page_url)) {
      return FALSE;
    }
  }

}
