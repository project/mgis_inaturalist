<?php

/**
 * @file
 * Migration de propriétés.
 */

/**
 * Code pays.
 *
 * Prendre en charge le code ISO 3166-1 alpha-3 de l'État où a eu lieu
 * l'observation.
 */
class MigrateBanabiomapStockPropCountry extends MigrateBanabiomapMigration {

  /**
   * Déclaration des paramètres de la migration.
   */
  public function __construct(array $arguments) {
    parent::__construct($arguments);

    $this->destination = new MigrateDestinationChadoProp('stock');

    $this->map = new MigrateSQLMap($this->machineName, [
      'id' => [
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'description' => t('Source ID'),
      ],
    ],
    MigrateDestinationChadoProp::getKeySchema('stock')
    );

    $this->addFieldMapping('stock_id', 'id')->sourceMigration('banabiomap_stock');
    $this->addFieldMapping('type_id')->defaultValue(MGIS_INATURALIST_CVTERM_INATURALIST_COUNTRY);
    $this->addFieldMapping('value', 'country_code');
  }

  /**
   * {@inheritdoc}
   */
  public function prepareRow($row) {
    if (parent::prepareRow($row) === FALSE) {
      return FALSE;
    }

    $row->country_code = $this->prepareCountry($row->country);
  }

  /**
   * Deal with country names.
   */
  protected function prepareCountry($country) {
    // Needed in order to call country_get_list().
    include_once DRUPAL_ROOT . '/includes/locale.inc';

    $faulty = [
      'Australie' => 'Australia',
      'Burma' => 'Myanmar',
      'china' => 'China',
      'Congo (Democratic Republic of the)' => 'Congo (Kinshasa)',
      'Corée du Sud' => 'South Korea',
      'Federated States of Micronesia' => 'Micronesia',
      'Guinea Ecuatorial' => 'Equatorial Guinea',
      'Hong Kong' => 'Hong Kong S.A.R., China',
      'La Réunion' => 'Reunion',
      'Nouvelle-Calédonie' => 'New Caledonia',
      'Polynésie Française' => 'French Polynesia',
      'Turquie' => 'Turkey',
      'United Republic of Tanzania' => 'Tanzania',
      'Viet Nam' => 'Vietnam',
    ];

    if (isset($faulty[$country])) {
      $country = $faulty[$country];
    }

    if ($country_code = array_search($country, country_get_list())) {
      $country_code = mgis_country_a2_to_a3($country_code);
    }
    else {
      $country_code = 'ZZZ';
    }

    return $country_code;
  }

}
