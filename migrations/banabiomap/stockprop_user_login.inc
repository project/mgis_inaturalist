<?php

/**
 * @file
 * Migration de propriétés.
 */

/**
 * Propriétés « user login ».
 */
class MigrateBanabiomapStockPropUserLogin extends MigrateBanabiomapMigration {

  /**
   * Déclaration des paramètres de la migration.
   */
  public function __construct(array $arguments) {
    parent::__construct($arguments);

    $this->destination = new MigrateDestinationChadoProp('stock');

    $this->map = new MigrateSQLMap($this->machineName, [
      'id' => [
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'description' => t('Source ID'),
      ],
    ],
    MigrateDestinationChadoProp::getKeySchema('stock')
    );

    $this->addFieldMapping('stock_id', 'id')->sourceMigration('banabiomap_stock');
    $this->addFieldMapping('type_id')->defaultValue(MGIS_INATURALIST_CVTERM_INATURALIST_LOGIN);
    $this->addFieldMapping('value', 'username');
  }

  /**
   * {@inheritdoc}
   */
  public function prepareRow($row) {
    if (parent::prepareRow($row) === FALSE) {
      return FALSE;
    }

    // Skip if no user login.
    if (empty($row->username)) {
      return FALSE;
    }

    // Hash user login: we only want to allow an iNaturalist user to find back
    // observations imported from iNaturalist, we do not care about their data.
    $row->username = sha1($row->username);
  }

}
