<?php

/**
 * @file
 * Migration de propriétés.
 */

/**
 * Propriétés « additional details ».
 */
class MigrateMgisStockPropAdditionalDetails extends MigrateMgisInaturalistMigration {

  /**
   * Déclaration des paramètres de la migration.
   */
  public function __construct(array $arguments) {
    parent::__construct($arguments);

    $this->destination = new MigrateDestinationChadoProp('stock');

    $this->map = new MigrateSQLMap($this->machineName, [
      'id' => [
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'description' => t('Source ID'),
      ],
    ],
    MigrateDestinationChadoProp::getKeySchema('stock')
    );

    $this->addFieldMapping('stock_id', 'id')->sourceMigration('inaturalist_stock');
    $this->addFieldMapping('type_id')->defaultValue(MGIS_INATURALIST_CVTERM_ADDITIONAL_DETAILS);
    $this->addFieldMapping('value', 'additional_details');
  }

  /**
   * {@inheritdoc}
   */
  public function prepareRow($row) {
    if (parent::prepareRow($row) === FALSE) {
      return FALSE;
    }

    // Skip if no additional details.
    if (empty($row->additional_details)) {
      return FALSE;
    }
  }

}
