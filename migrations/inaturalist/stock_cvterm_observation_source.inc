<?php

/**
 * @file
 * Migration de liens entre cvterm et stock.
 */

/**
 * Lien avec termes « banana observation source ».
 */
class MigrateMgisStockCvtermObservationSourceMigration extends MigrateMgisStockCvtermMigration {

  /**
   * Déclaration des paramètres de la migration.
   */
  public function __construct(array $arguments) {
    parent::__construct($arguments);

    $this->addFieldMapping('cvterm_id', 'observation_source')->sourceMigration('inaturalist_cvterm_observation_source');
  }

  /**
   * {@inheritdoc}
   */
  public function prepareRow($row) {
    if (parent::prepareRow($row) === FALSE) {
      return FALSE;
    }

    // All this only makes sense if observation_source is present.
    if (empty($row->observation_source)) {
      return FALSE;
    }

    // Correct some old values.
    if ($row->observation_source == 'Backyard garden') {
      $row->observation_source = 'Backyard garden / Arboretum';
    }
  }

}
