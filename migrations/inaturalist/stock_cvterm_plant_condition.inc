<?php

/**
 * @file
 * Migration de liens entre cvterm et stock.
 */

/**
 * Lien avec termes « banana plant condition ».
 */
class MigrateMgisStockCvtermPlantConditionMigration extends MigrateMgisStockCvtermMigration {

  /**
   * Déclaration des paramètres de la migration.
   */
  public function __construct(array $arguments) {
    parent::__construct($arguments);

    $this->addFieldMapping('cvterm_id', 'plant_condition')->sourceMigration('inaturalist_cvterm_plant_condition');
  }

  /**
   * {@inheritdoc}
   */
  public function prepareRow($row) {
    if (parent::prepareRow($row) === FALSE) {
      return FALSE;
    }

    // All this only makes sense if plant_condition is present.
    if (empty($row->plant_condition)) {
      return FALSE;
    }
  }

}
