<?php

/**
 * @file
 * Migration de cvterm (Banana plant condition) utilisés sur iNaturalist.
 */

/**
 * Migration des cvterm Banana plant condition.
 */
class MigrateMgisCvtermPlantConditionMigration extends Migration {

  /**
   * Déclaration des paramètres de la migration.
   */
  public function __construct(array $arguments) {
    parent::__construct($arguments);

    $csv_path = drupal_get_path('module', 'mgis_inaturalist') . '/data/plant_condition.csv';
    $columns = [['plant_condition', t('Banana plant condition')]];
    $this->source = new MigrateSourceCSV($csv_path, $columns);

    $this->destination = new MigrateDestinationChadoCvterm();

    $this->map = new MigrateSQLMap($this->machineName, [
      'plant_condition' => [
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'description' => t('Plant condition term from iNaturalist, used as ID'),
      ],
    ],
    MigrateDestinationChadoCvterm::getKeySchema()
    );

    $this->addFieldMapping('cv_id')->defaultValue(MGIS_INATURALIST_CV_PLANT_CONDITION);
    $this->addFieldMapping('name', 'plant_condition');
    $this->addFieldMapping('dbxref_id', 'plant_condition')->sourceMigration('inaturalist_dbxref_cvterm_plant_condition');
  }

}
