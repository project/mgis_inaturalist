<?php

/**
 * @file
 * Migration des photos depuis iNaturalist.
 */

/**
 * Migration des photos depuis iNaturalist.
 */
abstract class MigrateMgisInaturalistPictureMigration extends Migration {

  /**
   * Déclaration des paramètres de la migration.
   */
  public function __construct(array $arguments) {
    parent::__construct($arguments);

    $cache_directory = 'public://banana-natural-biodiversity-mapping';
    // List of all observations with a license, i.e. exclude "all rights
    // reserved" observations.
    $list_url = 'https://www.inaturalist.org/observations/project/banana-natural-biodiversity-mapping.json?license=any';
    $item_url = $cache_directory . '/photos/:id.json';

    $fields = [
      'id' => t('iNaturalist photo unique ID'),
      'observation_id' => t('iNaturalist observation unique ID'),
      'user_id' => t('iNaturalist user ID'),
      'native_photo_id' => 'iNaturalist photo native ID',
      'square_url' => t('Square photo URL'),
      'thumb_url' => t('Thumb photo URL'),
      'small_url' => t('Small photo URL'),
      'medium_url' => t('Medium photo URL'),
      'large_url' => t('Large photo URL'),
      'created_at' => t('Creation date'),
      'updated_at' => t('Modification date'),
      'native_page_url' => t('Photo page on iNaturalist'),
      'native_username'  => t('Contributor login'),
      'native_realname' => t('Contributor name'),
      'license' => t('Photo license (numeric code)'),
      'license_code' => t('Photo license (abbreviated name, like "CC-BY-NC")'),
      'license_name' => t('Photo license (actual name)'),
      'license_url' => t('License URL'),
      'attribution' => t('Attribution'),
      'type' => t('iNaturalist photo type, like "LocalPhoto"'),
      // Added in prepareRow().
      'file_uri' => t('Original file URL'),
      'destination_file' => t('File name used during migration'),
    ];
    $this->source = new MigrateSourceList(
      new InaturalistMigrateListJSON($list_url, $cache_directory, 'photo'),
      new MigrateItemJSON($item_url),
      $fields
    );
  }

  /**
   * {@inheritdoc}
   */
  public function prepareRow($row) {
    if (parent::prepareRow($row) === FALSE) {
      return FALSE;
    }

    $row->file_uri = 'https://static.inaturalist.org/photos/' . $row->id . '/original.jpeg';
    $row->destination_file = $row->id . '.jpg';
  }

}
