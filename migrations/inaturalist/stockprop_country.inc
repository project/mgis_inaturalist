<?php

/**
 * @file
 * Migration de propriétés.
 */

/**
 * Code pays.
 *
 * Prendre en charge le code ISO 3166-1 alpha-3 de l'État où a eu lieu
 * l'observation.
 */
class MigrateMgisStockPropCountry extends MigrateMgisInaturalistMigration {

  /**
   * Déclaration des paramètres de la migration.
   */
  public function __construct(array $arguments) {
    parent::__construct($arguments);

    $this->destination = new MigrateDestinationChadoProp('stock');

    $this->map = new MigrateSQLMap($this->machineName, [
      'id' => [
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'description' => t('Source ID'),
      ],
    ],
    MigrateDestinationChadoProp::getKeySchema('stock')
    );

    $this->addFieldMapping('stock_id', 'id')->sourceMigration('inaturalist_stock');
    $this->addFieldMapping('type_id')->defaultValue(MGIS_INATURALIST_CVTERM_INATURALIST_COUNTRY);
    $this->addFieldMapping('value', 'country_code');
  }

  /**
   * {@inheritdoc}
   */
  public function prepareRow($row) {
    if (parent::prepareRow($row) === FALSE) {
      return FALSE;
    }

    // Unknown country.
    if (empty($row->country_code)) {
      $row->country_code = 'ZZZ';
    }
  }

}
