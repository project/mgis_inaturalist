<?php

/**
 * @file
 * Migration des photos depuis iNaturalist.
 */

/**
 * Migration des photos depuis iNaturalist.
 */
class MigrateMgisPictureMigration extends MigrateMgisInaturalistPictureMigration {

  /**
   * Déclaration des paramètres de la migration.
   */
  public function __construct(array $arguments) {
    parent::__construct($arguments);

    $this->destination = new MigrateDestinationFile('file', 'MigrateFileUri');

    $this->map = new MigrateSQLMap($this->machineName, [
      'id' => [
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'description' => t('Source ID'),
      ],
    ],
    MigrateDestinationFile::getKeySchema()
    );

    $this->addFieldMapping('destination_dir')->defaultValue('public://pictures/banana-natural-biodiversity-mapping');
    $this->addFieldMapping('destination_file', 'destination_file');
    $this->addFieldMapping('value', 'large_url');
  }

}
