<?php

/**
 * @file
 * Migration de propriétés.
 */

/**
 * Propriétés « attribution ».
 */
class MigrateMgisDbxrefPropAttribution extends MigrateMgisInaturalistPictureMigration {

  /**
   * Déclaration des paramètres de la migration.
   */
  public function __construct(array $arguments) {
    parent::__construct($arguments);

    $this->destination = new MigrateDestinationChadoProp('dbxref');

    $this->map = new MigrateSQLMap($this->machineName, [
      'id' => [
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'description' => t('Source ID'),
      ],
    ],
    MigrateDestinationChadoProp::getKeySchema('dbxref')
    );

    $this->addFieldMapping('dbxref_id', 'id')->sourceMigration('inaturalist_dbxref_picture');
    $this->addFieldMapping('type_id')->defaultValue(MGIS_INATURALIST_CVTERM_PHOTO_ATTRIBUTION);
    $this->addFieldMapping('value', 'attribution');
  }

}
