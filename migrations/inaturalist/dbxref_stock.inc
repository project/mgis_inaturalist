<?php

/**
 * @file
 * Création d'un dbxref pour chaque observation iNaturalist.
 */

/**
 * Création d'un dbxref pour chaque observation iNaturalist.
 */
class MigrateMgisDbxrefStockMigration extends MigrateMgisInaturalistMigration {

  /**
   * Déclaration des paramètres de la migration.
   */
  public function __construct(array $arguments) {
    parent::__construct($arguments);

    $this->destination = new MigrateDestinationChadoDbxref();

    $this->map = new MigrateSQLMap($this->machineName, [
      'id' => [
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'description' => t('Source ID'),
      ],
    ],
    MigrateDestinationChadoDbxref::getKeySchema()
    );

    $this->addFieldMapping('db_id')->defaultValue(MGIS_INATURALIST_DB_INATURALIST_OBSERVATION);
    $this->addFieldMapping('accession', 'id');
    $this->addFieldMapping('version')->defaultValue('');
    $this->addFieldMapping('description')->defaultValue('');
  }

}
