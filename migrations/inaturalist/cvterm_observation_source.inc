<?php

/**
 * @file
 * Migration de cvterm (Banana observation source) utilisés sur iNaturalist.
 */

/**
 * Migration des cvterm Banana observation source.
 */
class MigrateMgisCvtermObservationSourceMigration extends Migration {

  /**
   * Déclaration des paramètres de la migration.
   */
  public function __construct(array $arguments) {
    parent::__construct($arguments);

    $csv_path = drupal_get_path('module', 'mgis_inaturalist') . '/data/observation_source.csv';
    $columns = [['observation_source', t('Banana observation source')]];
    $this->source = new MigrateSourceCSV($csv_path, $columns);

    $this->destination = new MigrateDestinationChadoCvterm();

    $this->map = new MigrateSQLMap($this->machineName, [
      'observation_source' => [
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'description' => t('Observation source name from iNaturalist, used as ID'),
      ],
    ],
    MigrateDestinationChadoCvterm::getKeySchema()
    );

    $this->addFieldMapping('cv_id')->defaultValue(MGIS_INATURALIST_CV_OBSERVATION_SOURCE);
    $this->addFieldMapping('name', 'observation_source');
    $this->addFieldMapping('dbxref_id', 'observation_source')->sourceMigration('inaturalist_dbxref_cvterm_observation_source');
  }

}
