<?php

/**
 * @file
 * Création d'un dbxref pour chaque cvterm.
 */

/**
 * Création d'un dbxref pour chaque terme « observation source ».
 */
class MigrateMgisDbxrefCvTermObservationSourceMigration extends Migration {

  /**
   * Déclaration des paramètres de la migration.
   */
  public function __construct(array $arguments) {
    parent::__construct($arguments);

    $csv_path = drupal_get_path('module', 'mgis_inaturalist') . '/data/observation_source.csv';
    $columns = [['observation_source', t('Banana observation source')]];
    $fields = ['dbxref_accession' => t('dbxref accession value')];
    $this->source = new MigrateSourceCSV($csv_path, $columns, [], $fields);

    $this->destination = new MigrateDestinationChadoDbxref();

    $this->map = new MigrateSQLMap($this->machineName, [
      'observation_source' => [
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'description' => t('Observation source name from iNaturalist, used as ID'),
      ],
    ],
    MigrateDestinationChadoDbxref::getKeySchema()
    );

    $this->addFieldMapping('db_id')->defaultValue(MGIS_INATURALIST_DB_INATURALIST);
    $this->addFieldMapping('accession', 'dbxref_accession');
    $this->addFieldMapping('version')->defaultValue('1');
    $this->addFieldMapping('description')->defaultValue('Auto-created by mgis_inaturalist');
  }

  /**
   * {@inheritdoc}
   */
  public function prepareRow($row) {
    if (parent::prepareRow($row) === FALSE) {
      return FALSE;
    }

    $row->dbxref_accession = 'BananaObservationSource:' . $row->observation_source;
  }

}
