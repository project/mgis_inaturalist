<?php

/**
 * @file
 * Migration depuis iNaturalist (stock).
 */

/**
 * Migration des observations (stock) depuis iNaturalist.
 */
class MigrateMgisStockMigration extends MigrateMgisInaturalistMigration {

  /**
   * Déclaration des paramètres de la migration.
   */
  public function __construct(array $arguments) {
    parent::__construct($arguments);

    $this->destination = new MigrateDestinationChadoStock();

    $this->map = new MigrateSQLMap($this->machineName, [
      'id' => [
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'description' => t('Source ID'),
      ],
    ],
    MigrateDestinationChadoStock::getKeySchema()
    );

    $this->addFieldMapping('type_id')->defaultValue(MGIS_INATURALIST_CVTERM_INATURALIST);
    $this->addFieldMapping('name', 'uniquename');
    $this->addFieldMapping('uniquename', 'uniquename');
    $this->addFieldMapping('description', 'description');
    $this->addFieldMapping('organism_id', 'organism_name')->sourceMigration('inaturalist_taxonomy');
  }

  /**
   * {@inheritdoc}
   */
  public function prepareRow($row) {
    if (parent::prepareRow($row) === FALSE) {
      return FALSE;
    }

    // Generate unique name for this observation.
    $row->uniquename = 'INAT' . $row->id;

    // Look for organism name to use for MGIS.
    $organism_name = $this->determineSourceOrganismName($row);
    $row->organism_name = ($this->isMappedOrganism($organism_name)) ? $organism_name : 'Musa';
  }

  /**
   * Determine source organism name.
   *
   * If "Banana botanical classification" custom field is present and has a
   * relevant value, attempt to use it as species name, otherwise try with
   * species_guess.
   */
  protected function determineSourceOrganismName($row) {
    if (empty($row->botanical_classification) || in_array($row->botanical_classification, ['Other', "Don't know"])) {
      return $row->species_guess;
    }

    return $row->botanical_classification;
  }

  /**
   * Is $organism_name a tracked organism?
   *
   * Returns TRUE if $organism_name is a value tracked by the
   * "inaturalist_taxonomy" migration.
   */
  protected function isMappedOrganism($organism_name) {
    return (bool) db_select('migrate_map_inaturalist_taxonomy', 'm')
      ->fields('m', ['destid1'])
      ->condition('sourceid1', $organism_name)
      ->execute()
      ->rowCount();
  }

}
