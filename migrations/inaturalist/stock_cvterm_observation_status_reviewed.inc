<?php

/**
 * @file
 * Migration de liens entre cvterm et stock.
 */

/**
 * Lien avec termes « observation status ».
 *
 * Par défaut, toutes les observations être catégorisées en reviewed = FALSE.
 */
class MigrateMgisStockCvtermObservationStatusReviewedMigration extends MigrateMgisStockCvtermMigration {

  /**
   * Déclaration des paramètres de la migration.
   */
  public function __construct(array $arguments) {
    parent::__construct($arguments);

    $this->addFieldMapping('cvterm_id')->defaultValue(MGIS_INATURALIST_CVTERM_REVIEWED);
    $this->addFieldMapping('is_not')->defaultValue(TRUE);
  }

}
