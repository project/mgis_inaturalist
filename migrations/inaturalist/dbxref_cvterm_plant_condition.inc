<?php

/**
 * @file
 * Création d'un dbxref pour chaque cvterm.
 */

/**
 * Création d'un dbxref pour chaque terme « plant condition ».
 */
class MigrateMgisDbxrefCvTermPlantConditionMigration extends Migration {

  /**
   * Déclaration des paramètres de la migration.
   */
  public function __construct(array $arguments) {
    parent::__construct($arguments);

    $csv_path = drupal_get_path('module', 'mgis_inaturalist') . '/data/plant_condition.csv';
    $columns = [['plant_condition', t('Banana plant condition')]];
    $fields = ['dbxref_accession' => t('dbxref accession value')];
    $this->source = new MigrateSourceCSV($csv_path, $columns, [], $fields);

    $this->destination = new MigrateDestinationChadoDbxref();

    $this->map = new MigrateSQLMap($this->machineName, [
      'plant_condition' => [
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'description' => t('Plant condition term from iNaturalist, used as ID'),
      ],
    ],
    MigrateDestinationChadoDbxref::getKeySchema()
    );

    $this->addFieldMapping('db_id')->defaultValue(MGIS_INATURALIST_DB_INATURALIST);
    $this->addFieldMapping('accession', 'dbxref_accession');
    $this->addFieldMapping('version')->defaultValue('1');
    $this->addFieldMapping('description')->defaultValue('Auto-created by mgis_inaturalist');
  }

  /**
   * {@inheritdoc}
   */
  public function prepareRow($row) {
    if (parent::prepareRow($row) === FALSE) {
      return FALSE;
    }

    $row->dbxref_accession = 'BananaPlantCondition:' . $row->plant_condition;
  }

}
