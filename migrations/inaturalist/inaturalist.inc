<?php

/**
 * @file
 * Migration depuis iNaturalist.
 */

/**
 * Migration des observations depuis iNaturalist.
 */
abstract class MigrateMgisInaturalistMigration extends Migration {

  /**
   * Déclaration des paramètres de la migration.
   */
  public function __construct(array $arguments) {
    parent::__construct($arguments);

    // List of all observations with a license, i.e. exclude "all rights
    // reserved" observations.
    $list_url = 'https://www.inaturalist.org/observations/project/banana-natural-biodiversity-mapping.json?license=any';
    $item_url = 'https://www.inaturalist.org/observations/:id.json';

    $cache_directory = 'public://banana-natural-biodiversity-mapping';
    $item_file = $cache_directory . '/observations/:id.json';

    $fields = [
      'id' => t('iNaturalist unique ID'),
      'observed_on' => t('Observation date (ISO 8601)'),
      'description' => t('Description for this observation'),
      'user' => t('iNaturalist user data'),
      'user_login' => t('User login'),
      'license' => t('Observation license'),
      'uri' => t('Observation URL on iNaturalist'),
      'observation_photos' => t('Observation photos'),
      'latitude' => t('Latitude'),
      'longitude' => t('Longitude'),
      'species_guess' => t('Most voted linnean name for this observation'),
      'taxon_id' => t('Taxon ID as referenced on iNaturalist'),
      'taxon' => t('iNaturalist taxon data'),
      'observation_field_values' => t('Custom fields'),
      'country_code' => t('Country code (ISO 3166-1 alpha-3)'),
      // Added in prepareRow().
      'uniquename' => t('Observation unique name'),
      'organism_name' => t('Organism name, either from observation field values or guessed name'),
      'plant_condition' => t('Banana plant condition'),
      'additional_details' => t('Additional details'),
      'observation_source' => t('Banana observation source'),
      'botanical_classification' => t('Banana botanical classification'),
    ];
    $this->source = new MigrateSourceList(
      new InaturalistMigrateListJSON($list_url, $cache_directory, 'observation'),
      new InaturalistMigrateItemJSON($item_url, $item_file),
      $fields
    );
  }

  /**
   * {@inheritdoc}
   */
  public function prepareRow($row) {
    if (parent::prepareRow($row) === FALSE) {
      return FALSE;
    }

    // Flatten observation field values.
    $observation_fields = [
      'Banana plant condition' => [
        'machine_name' => 'plant_condition',
        'value' => '',
      ],
      'Additional details' => [
        'machine_name' => 'additional_details',
        'value' => '',
      ],
      'Banana observation source' => [
        'machine_name' => 'observation_source',
        'value' => '',
      ],
      'Banana botanical classification' => [
        'machine_name' => 'botanical_classification',
        'value' => '',
      ],
    ];

    if (isset($row->observation_field_values)) {
      foreach ($row->observation_field_values as $field_value) {
        if (isset($field_value->observation_field->name) && isset($observation_fields[$field_value->observation_field->name]) && !empty($field_value->value)) {
          $observation_fields[$field_value->observation_field->name]['value'] = $field_value->value;
        }
      }
    }

    foreach ($observation_fields as $observation_field) {
      $row->{$observation_field['machine_name']} = $observation_field['value'];
    }
  }

}
