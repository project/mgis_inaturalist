<?php

/**
 * @file
 * Migration de la taxonomie pour iNaturalist.
 *
 * Aucun élément n'est crée dans la base : on se contente de fair
 * correspondre les éléments côté iNaturalist (champ personnalisé
 * « Banana botanical classification » et champ « species_guess ») avec
 * la taxonomie côté MGIS.
 */

/**
 * Mise en correspondance de la taxonomie.
 */
class MigrateMgisTaxonomyMigration extends Migration {

  /**
   * Déclaration des paramètres de la migration.
   */
  public function __construct(array $arguments) {
    parent::__construct($arguments);

    $csv_path = drupal_get_path('module', 'mgis_inaturalist') . '/data/taxonomy.csv';
    $columns = [
      0 => ['source_name', 'Source name from iNaturalist'],
      1 => ['genus', 'Genus name used for the MGIS project'],
      2 => ['species', 'Species name used for the MGIS project'],
    ];
    $options = ['header_rows' => 1];
    $this->source = new MigrateSourceCSV($csv_path, $columns, $options);

    $this->destination = new MigrateDestinationChadoOrganism();

    $this->map = new MigrateSQLMap($this->machineName, [
      'source_name' => [
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'description' => t('Source name from iNaturalist, used as ID'),
      ],
    ],
    MigrateDestinationChadoOrganism::getKeySchema()
    );

    $this->addFieldMapping('genus', 'genus');
    $this->addFieldMapping('species', 'species');
  }

}
