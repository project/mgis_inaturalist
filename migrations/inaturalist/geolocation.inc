<?php

/**
 * @file
 * Migration depuis iNaturalist (geolocation).
 */

/**
 * Migration des géolocalisations sur iNaturalist.
 */
class MigrateMgisGeolocationMigration extends MigrateMgisInaturalistMigration {

  /**
   * Déclaration des paramètres de la migration.
   */
  public function __construct(array $arguments) {
    parent::__construct($arguments);

    $this->destination = new MigrateDestinationChadoGeolocation();

    $this->map = new MigrateSQLMap($this->machineName, [
      'id' => [
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'description' => t('Source ID'),
      ],
    ],
    MigrateDestinationChadoGeolocation::getKeySchema()
    );

    $this->addFieldMapping('latitude', 'latitude');
    $this->addFieldMapping('longitude', 'longitude');
    $this->addFieldMapping('geodetic_datum')->defaultValue(MGIS_INATURALIST_GEODETIC_DATUM);
  }

}
