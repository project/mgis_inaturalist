<?php

/**
 * @file
 * Migration depuis iNaturalist (experiment).
 */

/**
 * Migration des observations (experiment) depuis iNaturalist.
 */
class MigrateMgisExperimentMigration extends MigrateMgisInaturalistMigration {

  /**
   * Déclaration des paramètres de la migration.
   */
  public function __construct(array $arguments) {
    parent::__construct($arguments);

    $this->destination = new MigrateDestinationChadoExperiment();

    $this->map = new MigrateSQLMap($this->machineName, [
      'id' => [
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'description' => t('Source ID'),
      ],
    ],
    MigrateDestinationChadoExperiment::getKeySchema()
    );

    $this->addFieldMapping('nd_geolocation_id', 'id')->sourceMigration('inaturalist_geolocation');
    $this->addFieldMapping('type_id')->defaultValue(MGIS_INATURALIST_CVTERM_OBSERVATION);
  }

}
