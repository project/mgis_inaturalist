<?php

/**
 * @file
 * mgis_inaturalist.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function mgis_inaturalist_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "facetapi" && $api == "facetapi_defaults") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function mgis_inaturalist_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_default_search_api_index().
 */
function mgis_inaturalist_default_search_api_index() {
  $items = array();
  $items['inaturalist_index'] = entity_import('search_api_index', '{
    "name" : "iNaturalist index",
    "machine_name" : "inaturalist_index",
    "description" : null,
    "server" : "elastic_search",
    "item_type" : "node",
    "options" : {
      "datasource" : { "bundles" : [ "chado_stock" ] },
      "index_directly" : 0,
      "cron_limit" : "50",
      "number_of_shards" : "1",
      "number_of_replicas" : "0",
      "fields" : {
        "accession_name" : { "type" : "text" },
        "accession_number" : { "type" : "string" },
        "accession_numbers" : { "type" : "list\\u003Cstring\\u003E" },
        "accession_status" : { "type" : "string" },
        "author" : { "type" : "integer", "entity_type" : "user" },
        "availability" : { "type" : "string" },
        "biological_status" : { "type" : "string" },
        "collection" : { "type" : "string" },
        "comments" : { "type" : "list\\u003Cinteger\\u003E", "entity_type" : "comment" },
        "contributor_login" : { "type" : "text" },
        "country_of_origin" : { "type" : "string" },
        "country_of_origin_a2_code" : { "type" : "string" },
        "country_of_origin_a3_code" : { "type" : "string" },
        "field_tags" : { "type" : "list\\u003Cinteger\\u003E", "entity_type" : "taxonomy_term" },
        "formtype" : { "type" : "string" },
        "genus" : { "type" : "string" },
        "group" : { "type" : "string" },
        "has_pictures" : { "type" : "string" },
        "holding_institute" : { "type" : "string" },
        "inaturalist_country" : { "type" : "string" },
        "nid" : { "type" : "integer" },
        "observation_hidden" : { "type" : "boolean" },
        "observation_reviewed" : { "type" : "boolean" },
        "observed_on" : { "type" : "date" },
        "picture_count" : { "type" : "integer" },
        "ploidy" : { "type" : "string" },
        "publications" : { "type" : "string" },
        "schema" : { "type" : "string" },
        "search_api_access_node" : { "type" : "list\\u003Cstring\\u003E" },
        "search_api_language" : { "type" : "string" },
        "species" : { "type" : "string" },
        "status" : { "type" : "boolean" },
        "stock_id" : { "type" : "integer" },
        "stock_name" : { "type" : "string" },
        "stock_type" : { "type" : "string" },
        "stock_uniquename" : { "type" : "string" },
        "storage" : { "type" : "list\\u003Cstring\\u003E" },
        "studies" : { "type" : "list\\u003Cinteger\\u003E", "entity_type" : "node" },
        "subgroup" : { "type" : "string" },
        "subspecies" : { "type" : "string" },
        "title" : { "type" : "text" },
        "type" : { "type" : "string" }
      },
      "data_alter_callbacks" : {
        "search_api_alter_bundle_filter" : {
          "status" : 0,
          "weight" : "-50",
          "settings" : { "default" : "1", "bundles" : [] }
        },
        "search_api_alter_add_url" : { "status" : 0, "weight" : "-49", "settings" : [] },
        "search_api_alter_add_aggregation" : { "status" : 0, "weight" : "-48", "settings" : [] },
        "search_api_alter_add_viewed_entity" : {
          "status" : 0,
          "weight" : "-47",
          "settings" : { "mode" : "full", "global_language_switch" : 0 }
        },
        "search_api_alter_add_hierarchy" : { "status" : 0, "weight" : "-46", "settings" : { "fields" : [] } },
        "search_api_alter_node_access" : { "status" : 1, "weight" : "-45", "settings" : [] },
        "search_api_alter_node_status" : { "status" : 1, "weight" : "-44", "settings" : [] },
        "mgis_inaturalist_accession_type_filter" : {
          "status" : 1,
          "weight" : "-43",
          "settings" : {
            "default" : "0",
            "accession_types" : { "49726" : "49726", "49750" : "49750", "49661" : "49661" }
          }
        }
      },
      "processors" : {
        "mgis_inaturalist_sha1" : {
          "status" : 1,
          "weight" : "0",
          "settings" : { "fields" : { "contributor_login" : true } }
        },
        "search_api_case_ignore" : {
          "status" : 0,
          "weight" : "0",
          "settings" : { "fields" : { "title" : true } }
        },
        "search_api_html_filter" : {
          "status" : 0,
          "weight" : "10",
          "settings" : {
            "fields" : { "title" : true, "accession_name" : true },
            "title" : 0,
            "alt" : 1,
            "tags" : "h1 = 5\\r\\nh2 = 3\\r\\nh3 = 2\\r\\nstrong = 2\\r\\nb = 2\\r\\nem = 1.5\\r\\nu = 1.5"
          }
        },
        "search_api_tokenizer" : {
          "status" : 0,
          "weight" : "20",
          "settings" : {
            "fields" : { "title" : true, "accession_name" : true },
            "spaces" : "[^[:alnum:]]",
            "ignorable" : "[\\u0027]"
          }
        },
        "search_api_stopwords" : {
          "status" : 0,
          "weight" : "30",
          "settings" : {
            "fields" : { "title" : true, "accession_name" : true },
            "file" : "",
            "stopwords" : "but\\r\\ndid\\r\\nthe this that those\\r\\netc"
          }
        },
        "search_api_porter_stemmer" : {
          "status" : 0,
          "weight" : "35",
          "settings" : {
            "fields" : { "title" : true, "accession_name" : true },
            "exceptions" : "texan=texa"
          }
        },
        "search_api_highlighting" : {
          "status" : 0,
          "weight" : "40",
          "settings" : {
            "prefix" : "\\u003Cstrong\\u003E",
            "suffix" : "\\u003C\\/strong\\u003E",
            "excerpt" : 1,
            "excerpt_length" : "256",
            "exclude_fields" : [],
            "highlight" : "always",
            "highlight_partial" : 0
          }
        }
      }
    },
    "enabled" : "1",
    "read_only" : "0",
    "rdf_mapping" : []
  }');
  return $items;
}
