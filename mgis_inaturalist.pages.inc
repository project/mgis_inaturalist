<?php

/**
 * @file
 * User pages.
 */

module_load_include('inc', 'migrate_ui', 'migrate_ui.pages');

/**
 * Simplified migrations management.
 *
 * @see migrate_ui_migrate_dashboard()
 */
function mgis_inaturalist_migrate_dashboard($form, &$form_state) {
  drupal_set_title(t('Migrate dashboard'));

  $build = [];

  $build['overview'] = [
    '#prefix' => '<div>',
    '#markup' => filter_xss_admin(migrate_overview()),
    '#suffix' => '</div>',
  ];

  $header = [
    'machinename' => ['data' => t('Group')],
    'status' => ['data' => t('Status')],
    'source_system' => ['data' => t('Source system')],
  ];

  $result = db_select('migrate_group', 'mg')
    ->fields('mg', ['name', 'title', 'arguments'])
    ->execute();
  $rows = [];
  foreach ($result as $group_row) {
    $row = [];
    $migration_result = db_select('migrate_status', 'ms')
      ->fields('ms', ['machine_name', 'status', 'arguments'])
      ->condition('group_name', $group_row->name)
      ->execute();
    if (!$migration_result) {
      continue;
    }
    $status = t('Idle');
    $idle = TRUE;
    $machine_names = [];
    foreach ($migration_result as $migration_row) {
      switch ($migration_row->status) {
        case MigrationBase::STATUS_IMPORTING:
          $status = t('Importing');
          $idle = FALSE;
          break;

        case MigrationBase::STATUS_ROLLING_BACK:
          $status = t('Rolling back');
          $idle = FALSE;
          break;

        case MigrationBase::STATUS_STOPPING:
          $status = t('Stopping');
          $idle = FALSE;
          break;
      }
      $machine_names[] = $migration_row->machine_name;
    }

    // If we're not in the middle of an operaton, what's the status of the
    // import?
    if ($idle) {
      $ready = TRUE;
      $complete = TRUE;
      foreach ($machine_names as $machine_name) {
        $migration = Migration::getInstance($machine_name);
        if (!$migration) {
          continue;
        }
        if (method_exists($migration, 'sourceCount') && method_exists($migration, 'processedCount')) {
          $source_count = $migration->sourceCount();
          $processed_count = $migration->processedCount();
          if ($processed_count > 0) {
            $ready = FALSE;
            if (!$complete) {
              break;
            }
          }
          if ($processed_count != $source_count) {
            $complete = FALSE;
            if (!$ready) {
              break;
            }
          }
        }
      }
      if ($ready) {
        $status = t('Ready to import');
      }
      elseif ($complete) {
        $status = t('Import complete');
      }
      else {
        $status = t('Import incomplete, not currently running');
      }
    }

    $row['status'] = $status;
    $row['machinename'] = $group_row->title;
    $arguments = unserialize($group_row->arguments);
    if (!empty($arguments['source_system'])) {
      $row['source_system'] = filter_xss_admin($arguments['source_system']);
    }
    else {
      $row['source_system'] = '';
    }
    $rows[$group_row->name] = $row;
  }

  $build['dashboard']['tasks'] = [
    '#type' => 'tableselect',
    '#header' => $header,
    '#options' => $rows,
    '#tree' => TRUE,
    '#empty' => t('No migration groups defined.'),
  ];
  $build['operations'] = _mgis_inaturalist_migrate_operations();

  $build['data_sources'] = [
    '#type' => 'fieldset',
    '#title' => t('Data sources'),
  ];
  $build['data_sources']['inaturalist'] = [
    '#type' => 'fieldset',
    '#title' => t('iNaturalist'),
    '#collapsible' => TRUE,
  ];
  $build['data_sources']['inaturalist']['inaturalist'] = [
    '#type' => 'item',
    '#markup' => t('All data will be pulled from the <a href="!url">Banana natural biodiversity mapping</a> project, via the iNaturalist API.', ['!url' => 'https://www.inaturalist.org/projects/banana-natural-biodiversity-mapping']),
  ];

  $build['data_sources']['banabiomap'] = [
    '#type' => 'fieldset',
    '#title' => t('Banabiomap'),
    '#collapsible' => TRUE,
  ];
  $build['data_sources']['banabiomap']['banabiomap'] = [
    '#type' => 'item',
    '#markup' => t('Current CSV file: <a href="!url">banabiomap.csv</a>.', ['!url' => file_create_url('public://banabiomap/banabiomap.csv')]),
  ];
  $build['data_sources']['banabiomap']['banabiomap_csv'] = [
    '#type' => 'file',
    '#title' => t('CSV file'),
    '#description' => t('Upload a new CSV file. Allowed extensions: csv.'),
  ];
  $build['data_sources']['banabiomap']['banabiomap_submit'] = [
    '#type' => 'actions',
  ];
  $build['data_sources']['banabiomap']['actions']['banabiomap_submit'] = [
    '#type' => 'submit',
    '#value' => t('Send'),
    '#validate' => ['mgis_inaturalist_migrate_banabiomap_validate'],
    '#submit' => ['mgis_inaturalist_migrate_banabiomap_submit'],
  ];

  $build['data_sources']['gbif'] = [
    '#type' => 'fieldset',
    '#title' => t('GBIF'),
    '#collapsible' => TRUE,
  ];
  $build['data_sources']['gbif']['gbif'] = [
    '#type' => 'item',
    '#markup' => t('<a href="!gbif-url">GBIF</a> occurrences, selected through <a href="!views-url">this interface</a>.', [
      '!gbif-url' => 'https://www.gbif.org',
      '!views-url' => url('gbif'),
    ]),
  ];

  return $build;
}

/**
 * Migration operations form.
 *
 * @return array
 *   Form.
 */
function _mgis_inaturalist_migrate_operations() {
  // Build the 'Update options' form.
  $operations = [
    '#type' => 'fieldset',
    '#title' => t('Operations'),
  ];

  $options = [
    'import_immediate' => t('Import'),
    'stop' => t('Stop'),
    'reset' => t('Reset'),
  ];
  $operations['operation'] = [
    '#type' => 'select',
    '#title' => t('Operation'),
    '#title_display' => 'invisible',
    '#options' => $options,
  ];
  $operations['submit'] = [
    '#type' => 'submit',
    '#value' => t('Execute'),
    '#validate' => ['migrate_ui_migrate_validate'],
    '#submit' => ['migrate_ui_migrate_submit'],
  ];
  $operations['description'] = [
    '#prefix' => '<p>',
    '#markup' => t(
      'Choose an operation to run on all selections above:
       <ul>
         <li>Import<br /> Imports all previously unprocessed records from the source, plus
             any records marked for update, into destination Drupal objects.</li>
         <li>Stop<br /> Cleanly interrupts any import or rollback processes that may
             currently be running.</li>
         <li>Reset<br /> Sometimes a process may fail to stop cleanly, and be
             left stuck in an Importing or Rolling Back status. Choose Reset to clear
             the status and permit other operations to proceed.</li>
       </ul>'
    ),
    '#postfix' => '</p>',
  ];
  // Only to avoid a warning in migrate_ui_migrate_submit().
  $operations['limit'] = [
    '#type' => 'value',
  ];

  return $operations;
}

/**
 * Validate banabiomap source.
 */
function mgis_inaturalist_migrate_banabiomap_validate($form, &$form_state) {
  $file = file_save_upload('banabiomap_csv', [
    'file_validate_extensions' => ['csv'],
  ]);
  // If the file passed validation:
  if ($file) {
    // Move the file into the Drupal file system.
    if ($file = file_move($file, 'public://banabiomap/banabiomap.csv', FILE_EXISTS_REPLACE)) {
      // Save the file for use in the submit handler.
      $form_state['storage']['file'] = $file;
    }
    else {
      form_set_error('banabiomap_csv', t("Failed to write the uploaded file to the site's file folder."));
    }
  }
  else {
    form_set_error('banabiomap_csv', t('No file was uploaded.'));
  }
}

/**
 * Banabiomap source submit handler.
 */
function mgis_inaturalist_migrate_banabiomap_submit($form, &$form_state) {
  $file = $form_state['storage']['file'];
  // Make the storage of the file permanent.
  $file->status = FILE_STATUS_PERMANENT;
  // Save file status.
  file_save($file);
  drupal_set_message(t('File successfully uploaded.'));
}
